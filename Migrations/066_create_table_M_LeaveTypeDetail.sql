
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_LeaveTypeDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LeaveTypeId] [int] NULL,
	[Name] [varchar](max) NULL,
	[Quota] [int] NULL,
 CONSTRAINT [PK_M_LeaveTypeDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[M_LeaveTypeDetail] ON 

INSERT [dbo].[M_LeaveTypeDetail] ([Id], [LeaveTypeId], [Name], [Quota]) VALUES (1, 6, N'Pernikahan Pekerja', 3)
INSERT [dbo].[M_LeaveTypeDetail] ([Id], [LeaveTypeId], [Name], [Quota]) VALUES (2, 6, N'Pernikahan Anak', 2)
INSERT [dbo].[M_LeaveTypeDetail] ([Id], [LeaveTypeId], [Name], [Quota]) VALUES (3, 6, N'Pernikahan Saudara Kandung', 1)
INSERT [dbo].[M_LeaveTypeDetail] ([Id], [LeaveTypeId], [Name], [Quota]) VALUES (4, 6, N'Istri Melahirkan/Keguguran', 2)
INSERT [dbo].[M_LeaveTypeDetail] ([Id], [LeaveTypeId], [Name], [Quota]) VALUES (5, 6, N'Pasangan atau Anak Meninggal/Kecelakaan Berat', 3)
INSERT [dbo].[M_LeaveTypeDetail] ([Id], [LeaveTypeId], [Name], [Quota]) VALUES (6, 6, N'Orang Tua Meninggal', 3)
INSERT [dbo].[M_LeaveTypeDetail] ([Id], [LeaveTypeId], [Name], [Quota]) VALUES (7, 6, N'Mertua/Menantu Meninggal', 3)
INSERT [dbo].[M_LeaveTypeDetail] ([Id], [LeaveTypeId], [Name], [Quota]) VALUES (8, 6, N'Saudara Kandung Meninggal', 3)
INSERT [dbo].[M_LeaveTypeDetail] ([Id], [LeaveTypeId], [Name], [Quota]) VALUES (9, 6, N'Pasangan/Anak Sakit Keras', 2)
INSERT [dbo].[M_LeaveTypeDetail] ([Id], [LeaveTypeId], [Name], [Quota]) VALUES (10, 6, N'Khitan/Baptis Anak', 2)
INSERT [dbo].[M_LeaveTypeDetail] ([Id], [LeaveTypeId], [Name], [Quota]) VALUES (11, 6, N'Orang Tua/Mertua Kecelakaan Berat/Sakit Keras', 2)
INSERT [dbo].[M_LeaveTypeDetail] ([Id], [LeaveTypeId], [Name], [Quota]) VALUES (12, 6, N'Anggota Keluarga Pekerja dalam 1 Rumah Meninggal', 1)
SET IDENTITY_INSERT [dbo].[M_LeaveTypeDetail] OFF
