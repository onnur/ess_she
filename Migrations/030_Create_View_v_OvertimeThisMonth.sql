Create view v_OvertimeThisMonth as
select Id,Nrp,[Date],StartTime,EndTime,Status,UserName,OvertimeWork,
(Datename(weekday,[Date])) as DayName,
(DATEDIFF(HOUR, StartTime, EndTime)) as HourTime, 
(DATEDIFF(MINUTE, StartTime, EndTime)) as MinuteTime 
from T_Overtime
where MONTH([Date]) = MONTH(GETDATE()) AND YEAR([Date]) = YEAR(GETDATE())
and Status = 'APPROVED'
