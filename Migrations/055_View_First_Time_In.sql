create view V_First_Date_In_Time as
SELECT PIN, min([Datetime]) AS [DateTime] FROM M_AttendanceMachine 
where MONTH([Datetime]) = MONTH(getdate())
and YEAR([Datetime]) = YEAR(getdate())
GROUP BY PIN,cast([DateTime] as date)