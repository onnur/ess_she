Alter Table T_LeaveAttachment Drop Column Id
Go

Alter Table T_LeaveAttachment
Add Id_new Int Identity(1, 1) Primary Key
Go

Exec sp_rename 'T_LeaveAttachment.Id_new', 'Id', 'Column'

