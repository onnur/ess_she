use ESS_ATTENDANCE_PROD
GO

ALTER TABLE T_Travels ADD [SummaryRejectReason] [varchar](255);


ALTER TABLE T_TravelExpenses ADD [SummaryRejectReason] [varchar](255);


ALTER TABLE T_Permits ADD [SummaryRejectReason] [varchar](255);


ALTER TABLE T_Overtime ADD [SummaryRejectReason] [varchar](255);


ALTER TABLE T_Leaves ADD [SummaryRejectReason] [varchar](255);



