﻿UPDATE M_Configs SET [Value] = null WHERE [Name] = 'ApproverLeave1';
UPDATE M_Configs SET [Value] = 'Yuwono Pratomo', [NRP] = '11001' WHERE [Name] = 'ApproverLeave2';

UPDATE M_Configs SET [Value] = null WHERE [Name] = 'ApproverOvertime1';
UPDATE M_Configs SET [Value] = 'Feny Febriana', [NRP] = '14001' WHERE [Name] = 'ApproverOvertime2';

UPDATE M_Configs SET [Value] = null WHERE [Name] = 'ApproverPermit1';
UPDATE M_Configs SET [Value] = 'Feny Febriana', [NRP] = '14001' WHERE [Name] = 'ApproverPermit2';

UPDATE M_Configs SET [Value] = null WHERE [Name] = 'ApproverTravel1';
UPDATE M_Configs SET [Value] = 'Irfan Muslimin Irvyant', [NRP] = '15013' WHERE [Name] = 'ApproverTravel2';
UPDATE M_Configs SET [Value] = 'Ferdiansyah Syafri', [NRP] = '06009' WHERE [Name] = 'ApproverTravel3';
UPDATE M_Configs SET [Value] = 'Agus Salam', [NRP] = '14014' WHERE [Name] = 'ApproverTravel4';
UPDATE M_Configs SET [Value] = 'Junya Ankyu', [NRP] = '16001' WHERE [Name] = 'ApproverTravel5';

UPDATE M_Configs SET [Value] = null WHERE [Name] = 'ApproverTer1';
UPDATE M_Configs SET [Value] = 'Irfan Muslimin Irvyant', [NRP] = '15013' WHERE [Name] = 'ApproverTer2';
UPDATE M_Configs SET [Value] = 'Ferdiansyah Syafri', [NRP] = '06009' WHERE [Name] = 'ApproverTer3';
UPDATE M_Configs SET [Value] = 'Agus Salam', [NRP] = '14014' WHERE [Name] = 'ApproverTer4';
