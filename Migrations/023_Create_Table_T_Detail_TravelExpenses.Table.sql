USE [EmployeeSelfService]
GO
/****** Object:  Table [dbo].[T_Detail_TravelExpenses]    Script Date: 2019-09-18 4:20:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Detail_TravelExpenses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TravelExpenseId] [int] NULL,
	[Date] [date] NULL,
	[LocationAccomodation] [varchar](max) NULL,
	[Allowance] [int] NULL,
	[Accomodation] [int] NULL,
	[Expense1] [int] NULL,
	[Remark1] [varchar](max) NULL,
	[Total] [int] NULL,
	[Expense2] [int] NULL,
	[Expense3] [int] NULL,
	[Expense4] [int] NULL,
	[Expense5] [int] NULL,
	[Expense6] [int] NULL,
	[Expense7] [int] NULL,
	[Remark2] [varchar](max) NULL,
	[Remark3] [varchar](max) NULL,
	[Remark4] [varchar](max) NULL,
	[Remark5] [varchar](max) NULL,
	[Remark6] [varchar](max) NULL,
	[Remark7] [varchar](max) NULL,
	[Location1] [varchar](max) NULL,
	[Location2] [varchar](max) NULL,
	[Location3] [varchar](max) NULL,
	[Location4] [varchar](max) NULL,
	[Location5] [varchar](max) NULL,
	[Location6] [varchar](max) NULL,
	[Location7] [varchar](max) NULL,
	[RemarkAccomodation] [varchar](max) NULL,
 CONSTRAINT [PK_T_Detail_TravelExpenses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
