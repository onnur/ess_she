ALTER TABLE T_Leaves ALTER COLUMN AppDate1 datetime;
ALTER TABLE T_Leaves ALTER COLUMN AppDate2 datetime;

ALTER TABLE T_Permits ALTER COLUMN AppDate1 datetime;
ALTER TABLE T_Permits ALTER COLUMN AppDate2 datetime;

ALTER TABLE T_Overtime ALTER COLUMN AppDate1 datetime;
ALTER TABLE T_Overtime ALTER COLUMN AppDate2 datetime;

ALTER TABLE T_Travels ALTER COLUMN AppDate1 datetime;
ALTER TABLE T_Travels ALTER COLUMN AppDate2 datetime;
ALTER TABLE T_Travels ALTER COLUMN AppDate3 datetime;
ALTER TABLE T_Travels ALTER COLUMN AppDate4 datetime;
ALTER TABLE T_Travels ALTER COLUMN AppDate5 datetime;


ALTER TABLE T_TravelExpenses ALTER COLUMN AppDate1 datetime;
ALTER TABLE T_TravelExpenses ALTER COLUMN AppDate2 datetime;
ALTER TABLE T_TravelExpenses ALTER COLUMN AppDate3 datetime;
ALTER TABLE T_TravelExpenses ALTER COLUMN AppDate4 datetime;
ALTER TABLE T_TravelExpenses ALTER COLUMN AppDate5 datetime;
