﻿ALTER TABLE T_Leaves 
	ADD AppName1 varchar(max),
		AppName2 varchar(max);

ALTER TABLE T_Permits
	ADD AppName1 varchar(max),
		AppName2 varchar(max);

ALTER TABLE T_Overtime
	ADD AppName1 varchar(max),
		AppName2 varchar(max);

ALTER TABLE T_Travels
	ADD AppName1 varchar(max),
		AppName2 varchar(max),
		AppName3 varchar(max),
		AppName4 varchar(max),
		AppName5 varchar(max);

ALTER TABLE T_TravelExpenses
	ADD AppName1 varchar(max),
		AppName2 varchar(max),
		AppName3 varchar(max),
		AppName4 varchar(max);