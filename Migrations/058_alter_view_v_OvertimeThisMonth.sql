alter view v_OvertimeThisMonth as
SELECT        Id, Nrp, StartDate, EndDate, StartTime, EndTime, Status, UserName, OverTimeWork, DATENAME(weekday, StartDate) AS DayName, DATEDIFF(HOUR, CAST(StartDate AS DATETIME) + CAST(StartTime AS DATETIME), 
                         CAST(EndDate AS DATETIME) + CAST(EndTime AS DATETIME)) AS HourTime, DATEDIFF(MINUTE, CAST(StartDate AS DATETIME) + CAST(StartTime AS DATETIME), CAST(EndDate AS DATETIME) + CAST(EndTime AS DATETIME)) 
                         AS MinuteTime, CreatedAt, UpdatedAt, GeneratedNumber
FROM            dbo.T_Overtime
WHERE        (Status = 'APPROVED')
