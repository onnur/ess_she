﻿ALTER TABLE [dbo].[M_AttendanceMachine]
	ADD [AttendanceStatusId] [int] NOT NULL DEFAULT 1

ALTER TABLE [dbo].[M_AttendanceMachine]  WITH CHECK ADD  CONSTRAINT [FK_Status_Attendance] FOREIGN KEY([AttendanceStatusId])
REFERENCES [dbo].[E_AttendanceStatus] ([Id])
GO

ALTER TABLE [dbo].[M_AttendanceMachine] CHECK CONSTRAINT [FK_Status_Attendance]
GO