USE [EmployeeSelfService]
GO
/****** Object:  Table [dbo].[T_LeaveHistory]    Script Date: 2019-09-04 8:47:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_LeaveHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[LeaveId] [int] NULL,
 CONSTRAINT [PK_T_LeaveHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
