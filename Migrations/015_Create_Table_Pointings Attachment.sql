USE [EmployeeSelfService]
GO
/****** Object:  Table [dbo].[T_LeaveAttachment]    Script Date: 2019-09-12 11:25:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_LeaveAttachment](
	[Id] [int] NULL,
	[LeaveId] [int] NULL,
	[Name] [varchar](max) NULL,
	[Caption] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_PointingAttachment]    Script Date: 2019-09-12 11:25:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_PointingAttachment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PointingId] [int] NULL,
	[Name] [varchar](max) NULL,
	[Caption] [varchar](max) NULL,
 CONSTRAINT [PK_T_PointingAttachment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_PointingSelectedActivity]    Script Date: 2019-09-12 11:25:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_PointingSelectedActivity](
	[Id] [int] NULL,
	[PointingId] [int] NULL,
	[PointingActivityId] [int] NULL,
	[PointingActivityName] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
