USE [ESS_ATTENDANCE]
GO
CREATE PROCEDURE spGetLeftQuotaByNrp @NRP varchar(100)
AS
IF (MONTH(getdate()) < 6)
BEGIN
	select SUM([QuotaLeft]) as  Quota,[Nrp] 
	from T_UserLeaveQuota
	where ([Year] = YEAR(getdate()) OR [Year] = (YEAR(getdate()) -1)) and Nrp = @NRP
	group by Nrp
END

ELSE
BEGIN
	select SUM([QuotaLeft]) as Quota,[Nrp] 
	from T_UserLeaveQuota
	where ([Year] = YEAR(getdate())) and Nrp = @NRP
	group by Nrp
END

