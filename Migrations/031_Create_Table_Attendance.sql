﻿CREATE TABLE [dbo].[R_Attendances](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NRP] [varchar](255) NOT NULL,
	[Date] [date] NULL,
	[DayType] [varchar](10) NULL,
	[CheckInTime] [time] NULL,
	[CheckOutTime] [time] NULL,
	[OverCheckInTime] [time] NULL,
	[OverCheckOutTime] [time] NULL,
	[AttendanceStatus] [int] NULL,
	[Remark] [varchar](255) NULL,
	[CreatedAt] [datetime] NOT NULL
 CONSTRAINT [PK_Attendance] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]