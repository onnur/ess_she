USE [EmployeeSelfService]
GO
/****** Object:  Table [dbo].[T_TravelExpenses]    Script Date: 2019-09-18 4:20:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_TravelExpenses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TravelRererence] [varchar](max) NULL,
	[TravelId] [int] NULL,
	[UserName] [varchar](max) NULL,
	[Nrp] [varchar](max) NULL,
	[SectionName] [varchar](max) NULL,
	[SectionId] [int] NULL,
	[Destination] [varchar](max) NULL,
	[TravelDate] [varchar](max) NULL,
	[ReportDate] [date] NULL,
	[TotalAllowance] [int] NULL,
	[TotalAccomodation] [int] NULL,
	[TotalExpense] [int] NULL,
	[TotalAll] [nchar](10) NULL,
	[TotalAdvance] [int] NULL,
	[PaidByCc] [int] NULL,
	[LessPaidToEmployee] [int] NULL,
	[MorePaidToCompany] [int] NULL,
	[App1] [varchar](max) NULL,
	[AppDate1] [date] NULL,
	[RejectReason1] [varchar](max) NULL,
	[AppStatus1] [varchar](max) NULL,
	[App2] [varchar](max) NULL,
	[AppDate2] [date] NULL,
	[RejectReason2] [varchar](max) NULL,
	[AppStatus2] [varchar](max) NULL,
	[App3] [varchar](max) NULL,
	[AppDate3] [date] NULL,
	[RejectReason3] [varchar](max) NULL,
	[AppStatus3] [varchar](max) NULL,
	[App4] [varchar](max) NULL,
	[AppDate4] [date] NULL,
	[RejectReason4] [varchar](max) NULL,
	[AppStatus4] [varchar](max) NULL,
	[App5] [varchar](max) NULL,
	[AppDate5] [date] NULL,
	[RejectReason5] [varchar](max) NULL,
	[AppStatus5] [varchar](max) NULL,
	[UserImage] [varchar](max) NULL,
	[Status] [varchar](max) NULL,
	[TotalPerDate] [int] NULL,
 CONSTRAINT [PK_T_TravelExpenses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
