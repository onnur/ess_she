INSERT INTO [dbo].[M_Configs]
           ([Name]
           ,[Value]
           ,[NRP])
     VALUES
           ('ApproverLeave3',
           'Yuwono Pratomo'
           ,'11001'),
		   ('ApproverOvertime3',
           'Yuwono Pratomo'
           ,'11001'),
           ('ApproverPermit3',
           'Yuwono Pratomo'
           ,'11001');

UPDATE [dbo].[M_Configs]
   SET [Value] = 'Achmad Riza Fanani Anzirwan',
      [NRP] = '19016'
 WHERE Name = 'ApproverLeave2';

 UPDATE [dbo].[M_Configs]
   SET [Value] = 'Achmad Riza Fanani Anzirwan',
      [NRP] = '19016'
 WHERE Name = 'ApproverOvertime2';

 UPDATE [dbo].[M_Configs]
   SET [Value] = 'Achmad Riza Fanani Anzirwan',
      [NRP] = '19016'
 WHERE Name = 'ApproverPermit2';


