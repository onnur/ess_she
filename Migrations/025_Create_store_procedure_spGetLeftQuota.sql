CREATE PROCEDURE spGetLeftQuota
AS
IF (MONTH(getdate()) < 6)
BEGIN
	select SUM([QuotaLeft]) as  Quota,[Nrp] 
	from T_UserLeaveQuota
	where ([Year] = YEAR(getdate()) OR [Year] = (YEAR(getdate()) -1)) 
	group by Nrp
END

ELSE
BEGIN
	select SUM([QuotaLeft]) as Quota,[Nrp] 
	from T_UserLeaveQuota
	where ([Year] = YEAR(getdate()))
	group by Nrp
END
GO