ALTER TABLE T_Leaves ADD App3 [varchar](255);
ALTER TABLE T_Leaves ADD AppDate3 datetime;
ALTER TABLE T_Leaves ADD App3Status [varchar](255);
ALTER TABLE T_Leaves ADD RejectReason3 [varchar](255);
ALTER TABLE T_Leaves ADD AppName3 [varchar](255);

ALTER TABLE T_Permits ADD App3 [varchar](255);
ALTER TABLE T_Permits ADD AppDate3 datetime;
ALTER TABLE T_Permits ADD App3Status [varchar](255);
ALTER TABLE T_Permits ADD RejectReason3 [varchar](255);
ALTER TABLE T_Permits ADD AppName3 [varchar](255);

ALTER TABLE T_Overtime ADD App3 [varchar](255);
ALTER TABLE T_Overtime ADD AppDate3 datetime;
ALTER TABLE T_Overtime ADD App3Status [varchar](255);
ALTER TABLE T_Overtime ADD RejectReason3 [varchar](255);
ALTER TABLE T_Overtime ADD AppName3 [varchar](255);
