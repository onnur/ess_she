﻿CREATE TABLE [dbo].[M_AttendanceSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [varchar](255) NULL,
	[Value] [varchar](255) NULL,
	[Default] [varchar](255) NULL
 CONSTRAINT [PK_AttendanceSetting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO [dbo].[M_AttendanceSettings]
           ([Key]
           ,[Value])
     VALUES
           ('WEEKEND',
           '["Sat", "Sun"]')

INSERT INTO [dbo].[M_AttendanceSettings]
           ([Key]
           ,[Value])
     VALUES
           ('START_TIME',
           '08:00')

INSERT INTO [dbo].[M_AttendanceSettings]
           ([Key]
           ,[Value])
     VALUES
           ('END_TIME',
           '17:00')