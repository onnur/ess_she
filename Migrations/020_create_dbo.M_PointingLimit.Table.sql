USE [EmployeeSelfService]
GO
/****** Object:  Table [dbo].[M_PointingLimit]    Script Date: 2019-09-13 5:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_PointingLimit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nrp] [varchar](max) NULL,
	[Limit] [int] NULL,
	[Balance] [int] NULL,
	[FromDate] [date] NULL,
	[ToDate] [date] NULL,
	[Utilization] [int] NULL,
 CONSTRAINT [PK_M_PointingLimit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
