ALTER TABLE [dbo].[T_Leaves] ADD GeneratedNumber varchar(MAX) NULL;

ALTER TABLE [dbo].[T_Overtime] ADD GeneratedNumber varchar(MAX) NULL;

ALTER TABLE [dbo].[T_Permits] ADD GeneratedNumber varchar(MAX) NULL;

ALTER TABLE [dbo].[T_Travels] ADD GeneratedNumber varchar(MAX) NULL;

ALTER TABLE [dbo].[T_TravelExpenses] ADD GeneratedNumber varchar(MAX) NULL;
