USE [ESS_ATTENDANCE]
GO
/****** Object:  StoredProcedure [dbo].[spGetLeftQuotaByNrp]    Script Date: 12/16/2019 10:25:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spGetLeftQuotaByNrp] @NRP varchar(100)
AS
IF (MONTH(getdate()) < 6)
BEGIN
	select SUM([QuotaLeft]) as  Quota,[Nrp] 
	from T_UserLeaveQuota
	where ([Year] = YEAR(getdate()) OR [Year] = (YEAR(getdate()) -1)) and Nrp = @NRP and LeaveTypeId = 1
	group by Nrp
END

ELSE
BEGIN
	select SUM([QuotaLeft]) as Quota,[Nrp] 
	from T_UserLeaveQuota
	where ([Year] = YEAR(getdate())) and Nrp = @NRP and LeaveTypeId = 1
	group by Nrp
END





