alter view V_First_Date_In_Time as
SELECT        Nrp, MIN(DateTime) AS DateTime
FROM            dbo.M_AttendanceMachine
WHERE        (MONTH(DateTime) = MONTH(GETDATE())) AND (YEAR(DateTime) = YEAR(GETDATE()))
GROUP BY Nrp, CAST(DateTime AS date)