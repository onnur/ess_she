USE [EmployeeSelfService]
GO
/****** Object:  Table [dbo].[T_PointingSelectedActivity]    Script Date: 2019-09-12 1:30:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_PointingSelectedActivity](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PointingId] [int] NULL,
	[PointingActivityId] [int] NULL,
	[PointingActivityName] [varchar](max) NULL,
 CONSTRAINT [PK_T_PointingSelectedActivity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
