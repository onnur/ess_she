﻿CREATE TABLE [dbo].[E_AttendanceStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
	[Value] [varchar](255) NULL,
 CONSTRAINT [PK_AttendanceStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO [dbo].[E_AttendanceStatus]
           ([Name]
           ,[Value])
     VALUES
           ('Present',
           'PRS')
INSERT INTO [dbo].[E_AttendanceStatus]
           ([Name]
           ,[Value])
     VALUES
           ('Off',
           'OFF')
INSERT INTO [dbo].[E_AttendanceStatus]
           ([Name]
           ,[Value])
     VALUES
           ('Leave',
           'LV')
INSERT INTO [dbo].[E_AttendanceStatus]
           ([Name]
           ,[Value])
     VALUES
           ('Travel',
           'TRV')