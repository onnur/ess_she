SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_SicknessAttachment](
	[SicknessId] [int] NULL,
	[Name] [varchar](max) NULL,
	[Caption] [varchar](max) NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_SicknessHistory]    Script Date: 12/5/2019 2:57:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_SicknessHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[SicknessId] [int] NULL,
	[YearUsedQuota] [varchar](max) NULL,
 CONSTRAINT [PK_T_SicknessHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Sickness]    Script Date: 12/5/2019 2:57:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Sickness](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[UserName] [varchar](max) NULL,
	[SectionId] [int] NULL,
	[SectionName] [varchar](max) NULL,
	[SicknessTypeId] [int] NULL,
	[SicknessTypeName] [varchar](max) NULL,
	[Reason] [varchar](max) NULL,
	[FormNumber] [varchar](max) NULL,
	[ReffNumber] [varchar](max) NULL,
	[Status] [varchar](max) NULL,
	[App1] [varchar](max) NULL,
	[App2] [varchar](max) NULL,
	[AppDate1] [datetime] NULL,
	[AppDate2] [datetime] NULL,
	[App1Status] [varchar](max) NULL,
	[App2Status] [varchar](max) NULL,
	[Nrp] [varchar](max) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[SicknessUsed] [int] NULL,
	[DatePickType] [varchar](max) NULL,
	[UserImage] [varchar](255) NULL,
	[RejectReason] [varchar](255) NULL,
	[RejectReason1] [varchar](255) NULL,
	[RejectReason2] [varchar](255) NULL,
	[Attachment] [varchar](255) NULL,
	[CreatedAt] [datetime] NULL,
	[UpdatedAt] [datetime] NULL,
	[GeneratedNumber] [varchar](max) NULL,
	[AppName1] [varchar](max) NULL,
	[AppName2] [varchar](max) NULL,
	[SummaryStatus] [varchar](255) NULL,
	[SummaryNumber] [varchar](255) NULL,
	[JvNumber] [varchar](255) NULL,
	[DcNumber] [varchar](255) NULL,
	[SummaryDate] [datetime] NULL,
	[SummaryRejectReason] [varchar](255) NULL,
	[App3] [varchar](255) NULL,
	[AppDate3] [datetime] NULL,
	[App3Status] [varchar](255) NULL,
	[RejectReason3] [varchar](255) NULL,
	[AppName3] [varchar](255) NULL,
	[Remark] [varchar](255) NULL,
	[SicknessTypeInfo] [int] NULL,
	[Location] [varchar](100) NULL,
 CONSTRAINT [PK_T_Sickness] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
