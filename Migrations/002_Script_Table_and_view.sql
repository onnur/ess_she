USE [EmployeeSelfService]
GO
/****** Object:  Table [dbo].[M_Dept]    Script Date: 9/4/2019 8:51:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_Dept](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nameDepartment] [varchar](50) NULL,
 CONSTRAINT [PK_M_Dept] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[M_FamilyStatus]    Script Date: 9/4/2019 8:51:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_FamilyStatus](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[famStatus] [varchar](50) NULL,
 CONSTRAINT [PK_M_FamilyStatus] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[M_Loan]    Script Date: 9/4/2019 8:51:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_Loan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nameLoan] [varchar](50) NULL,
 CONSTRAINT [PK_M_Loan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Approval_Loan]    Script Date: 9/4/2019 8:51:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Approval_Loan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TLoanId] [int] NULL,
	[Approval1] [varchar](50) NULL,
	[CreatedDateApproval1] [datetime] NULL,
	[Approval2] [varchar](50) NULL,
	[CreatedDateApproval2] [datetime] NULL,
	[Approval3] [varchar](50) NULL,
	[CreatedDateApproval3] [datetime] NULL,
	[Approval4] [varchar](50) NULL,
	[CreatedDateApproval] [datetime] NULL,
 CONSTRAINT [PK_T_Approval_Loan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Detail_Medical]    Script Date: 9/4/2019 8:51:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Detail_Medical](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_medical] [int] NULL,
	[PatientName] [varchar](max) NULL,
	[FamilyStatus] [int] NULL,
	[Doctor] [int] NULL,
	[Pharmacy] [int] NULL,
	[Admin] [int] NULL,
	[Remarks] [varchar](max) NULL,
 CONSTRAINT [PK_T_Detail_Medical] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Detail_Optical]    Script Date: 9/4/2019 8:51:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Detail_Optical](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_medical] [int] NULL,
	[PatientName] [varchar](max) NULL,
	[FamilyStatus] [int] NULL,
	[Frame] [int] NULL,
	[Lenses] [int] NULL,
	[Softlens] [int] NULL,
	[Remarks] [varchar](max) NULL,
 CONSTRAINT [PK_T_Detail_Optical] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Loan]    Script Date: 9/4/2019 8:51:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Loan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationNo] [varchar](50) NULL,
	[Department] [int] NULL,
	[IssueDate] [datetime] NULL,
	[Subject] [int] NULL,
	[Description] [varchar](max) NULL,
	[Amount] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_T_Loan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Medical]    Script Date: 9/4/2019 8:51:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Medical](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeName] [varchar](max) NULL,
	[NRP] [varchar](50) NULL,
	[Department] [int] NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK_T_Medical] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Optical]    Script Date: 9/4/2019 8:51:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Optical](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeName] [varchar](max) NULL,
	[NRP] [varchar](50) NULL,
	[Department] [int] NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK_T_Optical] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[vw_medical]    Script Date: 9/4/2019 8:51:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_medical]
AS
SELECT        dbo.T_Medical.id, dbo.T_Medical.EmployeeName, dbo.T_Medical.Department, dbo.T_Medical.DateCreated, dbo.T_Detail_Medical.PatientName, dbo.T_Detail_Medical.FamilyStatus, dbo.T_Detail_Medical.Doctor, 
                         dbo.T_Detail_Medical.Pharmacy, dbo.T_Detail_Medical.Admin, dbo.T_Detail_Medical.Remarks, dbo.T_Medical.NRP
FROM            dbo.T_Medical INNER JOIN
                         dbo.T_Detail_Medical ON dbo.T_Medical.id = dbo.T_Detail_Medical.id_medical

GO
/****** Object:  View [dbo].[vw_optical]    Script Date: 9/4/2019 8:51:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_optical]
AS
SELECT        dbo.T_Optical.id, dbo.T_Optical.EmployeeName, dbo.T_Optical.NRP, dbo.T_Optical.Department, dbo.T_Optical.DateCreated, dbo.T_Detail_Optical.PatientName, dbo.T_Detail_Optical.FamilyStatus, dbo.T_Detail_Optical.Frame, 
                         dbo.T_Detail_Optical.Lenses, dbo.T_Detail_Optical.Softlens, dbo.T_Detail_Optical.Remarks
FROM            dbo.T_Optical INNER JOIN
                         dbo.T_Detail_Optical ON dbo.T_Optical.id = dbo.T_Detail_Optical.id_medical

GO
SET IDENTITY_INSERT [dbo].[M_Dept] ON 

INSERT [dbo].[M_Dept] ([id], [nameDepartment]) VALUES (1, N'Administrasi')
INSERT [dbo].[M_Dept] ([id], [nameDepartment]) VALUES (2, N'Forklip')
INSERT [dbo].[M_Dept] ([id], [nameDepartment]) VALUES (3, N'SHE')
SET IDENTITY_INSERT [dbo].[M_Dept] OFF
SET IDENTITY_INSERT [dbo].[M_FamilyStatus] ON 

INSERT [dbo].[M_FamilyStatus] ([id], [famStatus]) VALUES (1, N'Wife')
INSERT [dbo].[M_FamilyStatus] ([id], [famStatus]) VALUES (2, N'Child')
SET IDENTITY_INSERT [dbo].[M_FamilyStatus] OFF
SET IDENTITY_INSERT [dbo].[M_Loan] ON 

INSERT [dbo].[M_Loan] ([id], [nameLoan]) VALUES (1, N'Car Ownership Program')
INSERT [dbo].[M_Loan] ([id], [nameLoan]) VALUES (2, N'Car Loan')
INSERT [dbo].[M_Loan] ([id], [nameLoan]) VALUES (3, N'Home Renovation Loan')
INSERT [dbo].[M_Loan] ([id], [nameLoan]) VALUES (4, N'Emergency Loan')
SET IDENTITY_INSERT [dbo].[M_Loan] OFF
SET IDENTITY_INSERT [dbo].[T_Detail_Medical] ON 

INSERT [dbo].[T_Detail_Medical] ([id], [id_medical], [PatientName], [FamilyStatus], [Doctor], [Pharmacy], [Admin], [Remarks]) VALUES (1, 1, N'Mudrikah', 1, 10, 10, 10, N'check up rutin')
SET IDENTITY_INSERT [dbo].[T_Detail_Medical] OFF
SET IDENTITY_INSERT [dbo].[T_Detail_Optical] ON 

INSERT [dbo].[T_Detail_Optical] ([id], [id_medical], [PatientName], [FamilyStatus], [Frame], [Lenses], [Softlens], [Remarks]) VALUES (1, 1, N'Istri', 1, 10, 10, 100, N'optical')
SET IDENTITY_INSERT [dbo].[T_Detail_Optical] OFF
SET IDENTITY_INSERT [dbo].[T_Loan] ON 

INSERT [dbo].[T_Loan] ([id], [ApplicationNo], [Department], [IssueDate], [Subject], [Description], [Amount], [CreatedDate]) VALUES (1, N'1', 1, CAST(N'2019-09-30 00:00:00.000' AS DateTime), 1, N'test cuy', 100, CAST(N'2019-10-10 00:00:00.000' AS DateTime))
INSERT [dbo].[T_Loan] ([id], [ApplicationNo], [Department], [IssueDate], [Subject], [Description], [Amount], [CreatedDate]) VALUES (2, N'2', 1, CAST(N'2019-12-12 00:00:00.000' AS DateTime), 2, N'test', 100, CAST(N'2019-09-02 14:40:46.233' AS DateTime))
INSERT [dbo].[T_Loan] ([id], [ApplicationNo], [Department], [IssueDate], [Subject], [Description], [Amount], [CreatedDate]) VALUES (6, N'56', 3, CAST(N'2019-11-28 00:00:00.000' AS DateTime), 2, N'asd', 100, NULL)
SET IDENTITY_INSERT [dbo].[T_Loan] OFF
SET IDENTITY_INSERT [dbo].[T_Medical] ON 

INSERT [dbo].[T_Medical] ([id], [EmployeeName], [NRP], [Department], [DateCreated]) VALUES (1, N'Warih', N'0001', 1, CAST(N'2019-09-27 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[T_Medical] OFF
SET IDENTITY_INSERT [dbo].[T_Optical] ON 

INSERT [dbo].[T_Optical] ([id], [EmployeeName], [NRP], [Department], [DateCreated]) VALUES (1, N'Warih Priantoro', N'001', 3, CAST(N'2019-10-10 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[T_Optical] OFF
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "T_Medical"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 141
               Right = 211
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "T_Detail_Medical"
            Begin Extent = 
               Top = 6
               Left = 249
               Bottom = 202
               Right = 419
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_medical'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_medical'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "T_Optical"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 176
               Right = 211
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "T_Detail_Optical"
            Begin Extent = 
               Top = 6
               Left = 249
               Bottom = 202
               Right = 419
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_optical'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_optical'
GO
