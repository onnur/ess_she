USE [master]
GO
/****** Object:  Database [ESS_ATTENDANCE]    Script Date: 2019-09-25 3:36:40 PM ******/
CREATE DATABASE [ESS_ATTENDANCE]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ESS_ATTENDANCE', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\ESS_ATTENDANCE.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ESS_ATTENDANCE_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\ESS_ATTENDANCE_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ESS_ATTENDANCE] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ESS_ATTENDANCE].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ESS_ATTENDANCE] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET ARITHABORT OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET  MULTI_USER 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ESS_ATTENDANCE] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ESS_ATTENDANCE] SET DELAYED_DURABILITY = DISABLED 
GO
USE [ESS_ATTENDANCE]
GO
/****** Object:  Table [dbo].[M_Configs]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_Configs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NULL,
	[Value] [varchar](max) NULL,
 CONSTRAINT [PK_M_Configs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[M_FamilyStatus]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_FamilyStatus](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[famStatus] [varchar](50) NULL,
 CONSTRAINT [PK_M_FamilyStatus] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[M_Hardship]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_Hardship](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[City] [varchar](max) NULL,
	[IsHardship] [int] NULL,
 CONSTRAINT [PK_M_Hardship] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[M_HolidayDate]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_HolidayDate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NULL,
	[Date] [date] NULL,
	[IsReligionDay] [bit] NULL DEFAULT ('FALSE'),
 CONSTRAINT [PK_M_HolidayDate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[M_LeaveType]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_LeaveType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NULL,
	[Quota] [int] NULL,
	[Code] [varchar](255) NULL,
 CONSTRAINT [PK_M_AnnualType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SectionDepartments]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SectionDepartments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NULL,
	[DepartmentId] [int] NULL,
 CONSTRAINT [PK_SectionDepartment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Attachment]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Attachment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_attach] [int] NULL,
	[FileName] [varchar](max) NULL,
	[Flag] [varchar](50) NULL,
 CONSTRAINT [PK_T_Attachment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Detail_TravelExpenses]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Detail_TravelExpenses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TravelExpenseId] [int] NULL,
	[Date] [date] NULL,
	[LocationAccomodation] [varchar](max) NULL,
	[Allowance] [int] NULL,
	[Accomodation] [int] NULL,
	[Expense1] [int] NULL,
	[Remark1] [varchar](max) NULL,
	[Total] [int] NULL,
	[Expense2] [int] NULL,
	[Expense3] [int] NULL,
	[Expense4] [int] NULL,
	[Expense5] [int] NULL,
	[Expense6] [int] NULL,
	[Expense7] [int] NULL,
	[Remark2] [varchar](max) NULL,
	[Remark3] [varchar](max) NULL,
	[Remark4] [varchar](max) NULL,
	[Remark5] [varchar](max) NULL,
	[Remark6] [varchar](max) NULL,
	[Remark7] [varchar](max) NULL,
	[Location1] [varchar](max) NULL,
	[Location2] [varchar](max) NULL,
	[Location3] [varchar](max) NULL,
	[Location4] [varchar](max) NULL,
	[Location5] [varchar](max) NULL,
	[Location6] [varchar](max) NULL,
	[Location7] [varchar](max) NULL,
	[RemarkAccomodation] [varchar](max) NULL,
 CONSTRAINT [PK_T_Detail_TravelExpenses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_LeaveAttachment]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_LeaveAttachment](
	[LeaveId] [int] NULL,
	[Name] [varchar](max) NULL,
	[Caption] [varchar](max) NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_LeaveHistory]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_LeaveHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[LeaveId] [int] NULL,
 CONSTRAINT [PK_T_LeaveHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_Leaves]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Leaves](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[UserName] [varchar](max) NULL,
	[SectionId] [int] NULL,
	[SectionName] [varchar](max) NULL,
	[LeaveTypeId] [int] NULL,
	[LeaveTypeName] [varchar](max) NULL,
	[Reason] [varchar](max) NULL,
	[FormNumber] [varchar](max) NULL,
	[ReffNumber] [varchar](max) NULL,
	[Status] [varchar](max) NULL,
	[App1] [varchar](max) NULL,
	[App2] [varchar](max) NULL,
	[AppDate1] [date] NULL,
	[AppDate2] [date] NULL,
	[App1Status] [varchar](max) NULL,
	[App2Status] [varchar](max) NULL,
	[Nrp] [varchar](max) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[LeaveUsed] [int] NULL,
	[DatePickType] [varchar](max) NULL,
	[UserImage] [varchar](255) NULL,
	[RejectReason] [varchar](255) NULL,
	[RejectReason1] [varchar](255) NULL,
	[RejectReason2] [varchar](255) NULL,
	[Attachment] [varchar](255) NULL,
 CONSTRAINT [PK_T_Leaves] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Overtime]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Overtime](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](max) NULL,
	[Nrp] [varchar](max) NULL,
	[SectionName] [varchar](max) NULL,
	[Date] [date] NULL,
	[StartTime] [time](7) NULL,
	[EndTime] [time](7) NULL,
	[OverTimeWork] [varchar](max) NULL,
	[App1] [varchar](max) NULL,
	[AppStatus1] [varchar](max) NULL,
	[AppDate1] [date] NULL,
	[RejectReason1] [varchar](max) NULL,
	[App2] [varchar](max) NULL,
	[AppStatus2] [varchar](max) NULL,
	[AppDate2] [date] NULL,
	[RejectReason2] [varchar](max) NULL,
	[Status] [varchar](max) NULL,
	[UserImage] [varchar](255) NULL,
 CONSTRAINT [PK_T_Overtime] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Permits]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Permits](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](max) NULL,
	[Nrp] [varchar](max) NULL,
	[SectionName] [varchar](max) NULL,
	[Date] [date] NULL,
	[StartTime] [time](7) NULL,
	[EndTime] [time](7) NULL,
	[Remark] [varchar](max) NULL,
	[UserImage] [varchar](max) NULL,
	[Utilities] [varchar](max) NULL,
	[App1] [varchar](max) NULL,
	[AppStatus1] [varchar](max) NULL,
	[AppDate1] [date] NULL,
	[RejectReason1] [varchar](max) NULL,
	[App2] [varchar](max) NULL,
	[AppStatus2] [varchar](max) NULL,
	[AppDate2] [date] NULL,
	[RejectReason2] [varchar](max) NULL,
	[Status] [varchar](max) NULL,
 CONSTRAINT [PK_T_Permits] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_TravelExpense_Attachment]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_TravelExpense_Attachment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TravelExpenseId] [int] NULL,
	[Name] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
 CONSTRAINT [PK_T_TravelExpense_Attachment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_TravelExpenses]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_TravelExpenses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TravelRererence] [varchar](max) NULL,
	[TravelId] [int] NULL,
	[UserName] [varchar](max) NULL,
	[Nrp] [varchar](max) NULL,
	[SectionName] [varchar](max) NULL,
	[SectionId] [int] NULL,
	[Destination] [varchar](max) NULL,
	[TravelDate] [varchar](max) NULL,
	[ReportDate] [date] NULL,
	[TotalAllowance] [int] NULL,
	[TotalAccomodation] [int] NULL,
	[TotalExpense] [int] NULL,
	[TotalAll] [nchar](10) NULL,
	[TotalAdvance] [int] NULL,
	[PaidByCc] [int] NULL,
	[LessPaidToEmployee] [int] NULL,
	[MorePaidToCompany] [int] NULL,
	[App1] [varchar](max) NULL,
	[AppDate1] [date] NULL,
	[RejectReason1] [varchar](max) NULL,
	[AppStatus1] [varchar](max) NULL,
	[App2] [varchar](max) NULL,
	[AppDate2] [date] NULL,
	[RejectReason2] [varchar](max) NULL,
	[AppStatus2] [varchar](max) NULL,
	[App3] [varchar](max) NULL,
	[AppDate3] [date] NULL,
	[RejectReason3] [varchar](max) NULL,
	[AppStatus3] [varchar](max) NULL,
	[App4] [varchar](max) NULL,
	[AppDate4] [date] NULL,
	[RejectReason4] [varchar](max) NULL,
	[AppStatus4] [varchar](max) NULL,
	[App5] [varchar](max) NULL,
	[AppDate5] [date] NULL,
	[RejectReason5] [varchar](max) NULL,
	[AppStatus5] [varchar](max) NULL,
	[UserImage] [varchar](max) NULL,
	[Status] [varchar](max) NULL,
	[TotalPerDate] [int] NULL,
 CONSTRAINT [PK_T_TravelExpenses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Travels]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Travels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](max) NULL,
	[Nrp] [varchar](max) NULL,
	[SectionName] [varchar](max) NULL,
	[SectionId] [int] NULL,
	[Title] [varchar](max) NULL,
	[TravelCategoryId] [int] NULL,
	[TravelCategoryName] [varchar](max) NULL,
	[CityOrCountry] [varchar](max) NULL,
	[Company] [varchar](max) NULL,
	[Purpose] [varchar](max) NULL,
	[DateLeaving] [date] NULL,
	[DateReturning] [date] NULL,
	[Period] [int] NULL,
	[AdvanceAmount] [int] NULL,
	[App1] [varchar](max) NULL,
	[AppDate1] [date] NULL,
	[RejectReason1] [varchar](max) NULL,
	[AppStatus1] [varchar](max) NULL,
	[App2] [varchar](max) NULL,
	[AppDate2] [date] NULL,
	[RejectReason2] [varchar](max) NULL,
	[AppStatus2] [varchar](max) NULL,
	[App3] [varchar](max) NULL,
	[AppDate3] [date] NULL,
	[RejectReason3] [varchar](max) NULL,
	[AppStatus3] [varchar](max) NULL,
	[App4] [varchar](max) NULL,
	[AppDate4] [date] NULL,
	[RejectReason4] [varchar](max) NULL,
	[AppStatus4] [varchar](max) NULL,
	[ReferenceNumber] [varchar](max) NULL,
	[IsExpatriate] [int] NULL,
	[Allowance] [int] NULL,
	[UserImage] [varchar](max) NULL,
	[Status] [varchar](max) NULL,
	[Passport] [varchar](max) NULL,
	[Visa] [varchar](max) NULL,
	[Attachment] [varchar](max) NULL,
	[App5] [varchar](max) NULL,
	[AppDate5] [date] NULL,
	[RejectReason5] [varchar](max) NULL,
	[AppStatus5] [varchar](max) NULL,
	[AirTicket] [int] NULL,
	[AirportTax] [int] NULL,
	[TrainBus] [int] NULL,
	[TaxiFare] [int] NULL,
	[TollFee] [int] NULL,
	[RentalCar] [int] NULL,
	[Hotel] [int] NULL,
	[AllowanceEstimation] [int] NULL,
	[Other1] [int] NULL,
	[Other2] [int] NULL,
	[Other1Text] [varchar](max) NULL,
	[Other2Text] [varchar](max) NULL,
 CONSTRAINT [PK_T_Travel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_UserLeaveQuota]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_UserLeaveQuota](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[QuotaLeft] [int] NULL,
	[Year] [varchar](max) NULL,
	[Nrp] [varchar](max) NULL,
 CONSTRAINT [PK_T_UserLeaveQuota] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[v_CurrentQuota]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	Create View [dbo].[v_CurrentQuota] as
	select [Year],QuotaLeft,[Nrp] 
	from T_UserLeaveQuota
	where ([Year] = YEAR(getdate()) OR [Year] = (YEAR(getdate()) -1)) 


GO
/****** Object:  View [dbo].[v_OvertimeThisMonth]    Script Date: 2019-09-25 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[v_OvertimeThisMonth] as
select Id,Nrp,[Date],StartTime,EndTime,Status,UserName,OvertimeWork,
(Datename(weekday,[Date])) as DayName,
(DATEDIFF(HOUR, StartTime, EndTime)) as HourTime, 
(DATEDIFF(MINUTE, StartTime, EndTime)) as MinuteTime 
from T_Overtime
where MONTH([Date]) = MONTH(GETDATE()) AND YEAR([Date]) = YEAR(GETDATE())
and Status = 'APPROVED'


GO
SET IDENTITY_INSERT [dbo].[M_Configs] ON 

INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (1, N'AnnualLeavesQuota', N'12')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (2, N'ApproverLeave1', N'Asep')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (3, N'ApproverLeave2', N'Yuwono')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (4, N'ApproverOvertime1', N'Tata')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (5, N'ApproverOvertime2', N'Jaja')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (6, N'ApproverPermit1', N'Rara')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (7, N'ApproverPermit2', N'Dadang')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (8, N'ApproverPointing1', N'Lala')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (9, N'ApproverPointing2', N'Rianti')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (10, N'ApproverTravel1', N'Tata')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (11, N'ApproverTravel2', N'Jaja')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (12, N'ApproverTravel3', N'Sopia')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (13, N'ApproverTravel4', N'Erwa')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (14, N'ApproverTravel5', N'Roni')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (1010, N'ApproverTer1', N'Tatag')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (1011, N'ApproverTer2', N'Jajag')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (1012, N'ApproverTer3', N'Sopiag')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (1013, N'ApproverTer4', N'Erwag')
INSERT [dbo].[M_Configs] ([Id], [Name], [Value]) VALUES (1014, N'ApproverTer5', N'Ronig')
SET IDENTITY_INSERT [dbo].[M_Configs] OFF
SET IDENTITY_INSERT [dbo].[M_FamilyStatus] ON 

INSERT [dbo].[M_FamilyStatus] ([id], [famStatus]) VALUES (1, N'Wife')
INSERT [dbo].[M_FamilyStatus] ([id], [famStatus]) VALUES (2, N'Child')
SET IDENTITY_INSERT [dbo].[M_FamilyStatus] OFF
SET IDENTITY_INSERT [dbo].[M_HolidayDate] ON 

INSERT [dbo].[M_HolidayDate] ([Id], [Name], [Date], [IsReligionDay]) VALUES (1, N'Libur Waisak', CAST(N'2019-09-04' AS Date), 1)
INSERT [dbo].[M_HolidayDate] ([Id], [Name], [Date], [IsReligionDay]) VALUES (2, N'Libur Idul Fitri', CAST(N'2019-09-12' AS Date), 1)
INSERT [dbo].[M_HolidayDate] ([Id], [Name], [Date], [IsReligionDay]) VALUES (3, N'Hari Buruh', CAST(N'2019-10-01' AS Date), 0)
INSERT [dbo].[M_HolidayDate] ([Id], [Name], [Date], [IsReligionDay]) VALUES (4, N'Hari Pancasila', CAST(N'2019-10-10' AS Date), 0)
INSERT [dbo].[M_HolidayDate] ([Id], [Name], [Date], [IsReligionDay]) VALUES (5, N'Hari Kemerdekaan', CAST(N'2019-08-17' AS Date), 0)
INSERT [dbo].[M_HolidayDate] ([Id], [Name], [Date], [IsReligionDay]) VALUES (6, N'Cuti Masal Idul Fitri', CAST(N'2019-09-13' AS Date), 0)
SET IDENTITY_INSERT [dbo].[M_HolidayDate] OFF
SET IDENTITY_INSERT [dbo].[M_LeaveType] ON 

INSERT [dbo].[M_LeaveType] ([Id], [Name], [Quota], [Code]) VALUES (1, N'Cuti Tahunan', 12, NULL)
INSERT [dbo].[M_LeaveType] ([Id], [Name], [Quota], [Code]) VALUES (3, N'Cuti Besar', 3, NULL)
INSERT [dbo].[M_LeaveType] ([Id], [Name], [Quota], [Code]) VALUES (4, N'Cuti Hamil dan Keguguran', NULL, NULL)
INSERT [dbo].[M_LeaveType] ([Id], [Name], [Quota], [Code]) VALUES (5, N'Cuti Haid', NULL, NULL)
INSERT [dbo].[M_LeaveType] ([Id], [Name], [Quota], [Code]) VALUES (6, N'Cuti Tidak Masuk Kerja', NULL, NULL)
INSERT [dbo].[M_LeaveType] ([Id], [Name], [Quota], [Code]) VALUES (7, N'Cuti Khusus', NULL, NULL)
INSERT [dbo].[M_LeaveType] ([Id], [Name], [Quota], [Code]) VALUES (8, N'Cuti Meninggalkan Pekerjaan Diluar Tanggungan Perusahaan', NULL, NULL)
INSERT [dbo].[M_LeaveType] ([Id], [Name], [Quota], [Code]) VALUES (9, N'Sickness', NULL, NULL)
SET IDENTITY_INSERT [dbo].[M_LeaveType] OFF
SET IDENTITY_INSERT [dbo].[SectionDepartments] ON 

INSERT [dbo].[SectionDepartments] ([Id], [Name], [DepartmentId]) VALUES (1, N'Engine Section', 1)
INSERT [dbo].[SectionDepartments] ([Id], [Name], [DepartmentId]) VALUES (2, N'Torkflow Section', 1)
SET IDENTITY_INSERT [dbo].[SectionDepartments] OFF
SET IDENTITY_INSERT [dbo].[T_Detail_TravelExpenses] ON 

INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (1, 1, CAST(N'2019-09-27' AS Date), N'asd', 234, 234, 3432, N'sfsd', 3900, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'sdf', NULL, NULL, NULL, NULL, NULL, NULL, N'sdf')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (2, 1, CAST(N'2019-09-28' AS Date), N'sdf', 324, 234, NULL, NULL, 558, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'sdf')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (3, 1, CAST(N'2019-09-29' AS Date), N'sdf', 324, 234, NULL, NULL, 558, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'sdfsd')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (4, 2, CAST(N'2019-09-27' AS Date), N'asd', 234, 23, 324, N'sdf', 581, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'sdf', NULL, NULL, NULL, NULL, NULL, NULL, N'sdf')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (5, 2, CAST(N'2019-09-28' AS Date), N'sdf', 234, 234, 234, N'sdf', 702, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'sdf', NULL, NULL, NULL, NULL, NULL, NULL, N'sdf')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (6, 2, CAST(N'2019-09-29' AS Date), N'', 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (7, 3, CAST(N'2019-09-27' AS Date), N'test', 1, 2, 2, N'dsfsdfds', 3, 4, NULL, NULL, NULL, NULL, NULL, N'sdfsd', NULL, NULL, NULL, NULL, NULL, N'sdfdsf', N'sdfdsf', NULL, NULL, NULL, NULL, NULL, N'test')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (8, 3, CAST(N'2019-09-28' AS Date), N'test2', 4, 1, 3, N'sdfsdf', 8, 7, NULL, NULL, NULL, NULL, NULL, N'sdfdsf', NULL, NULL, NULL, NULL, NULL, N'test2', N'ghjgh', NULL, NULL, NULL, NULL, NULL, N'test')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (9, 3, CAST(N'2019-09-29' AS Date), N'tes3', 2, 3, 5, N'sdfdsfdsf', 5, 8, NULL, NULL, NULL, NULL, NULL, N'NUsdfsLL', NULL, NULL, NULL, NULL, NULL, N'sdfdsf', N'NghjghULL', NULL, NULL, NULL, NULL, NULL, N'test3')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (30, 4, CAST(N'2019-09-01' AS Date), N'location 1edit', 1166, 1266, 1166, N'remarkex166', 4864, 1266, NULL, NULL, NULL, NULL, NULL, N'remarkex266', NULL, NULL, NULL, NULL, NULL, N'locationex166', N'locationex266', NULL, NULL, NULL, NULL, NULL, N'remark1edit')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (31, 4, CAST(N'2019-09-18' AS Date), N'erew', 66, 88, 46, N'fghgf', 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ghj', NULL, NULL, NULL, NULL, NULL, NULL, N'gfhjg')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (33, 5, CAST(N'2019-09-16' AS Date), N'location 1', 200000, 500, 200000, N'makan', 400500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'location a', NULL, NULL, NULL, NULL, NULL, NULL, N'remark')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (35, 5, CAST(N'2019-09-17' AS Date), N'location 2', 500000, 1000000, NULL, NULL, 1500000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'test2')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (37, 5, CAST(N'2019-09-18' AS Date), N'location 3', 4000000, 600000, 500000, N'test gojek', 5900000, 600000, 200000, NULL, NULL, NULL, NULL, N'taxi', N'obat', NULL, NULL, NULL, NULL, N'site', N'site', N'site', NULL, NULL, NULL, NULL, N'test')
INSERT [dbo].[T_Detail_TravelExpenses] ([Id], [TravelExpenseId], [Date], [LocationAccomodation], [Allowance], [Accomodation], [Expense1], [Remark1], [Total], [Expense2], [Expense3], [Expense4], [Expense5], [Expense6], [Expense7], [Remark2], [Remark3], [Remark4], [Remark5], [Remark6], [Remark7], [Location1], [Location2], [Location3], [Location4], [Location5], [Location6], [Location7], [RemarkAccomodation]) VALUES (41, 6, CAST(N'2019-09-07' AS Date), N'asdasd', 0, 0, 200000, N'makan', 200000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'location a', NULL, NULL, NULL, NULL, NULL, NULL, N'')
SET IDENTITY_INSERT [dbo].[T_Detail_TravelExpenses] OFF
SET IDENTITY_INSERT [dbo].[T_LeaveHistory] ON 

INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (3, CAST(N'2019-11-09' AS Date), 1)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (4, CAST(N'2019-09-28' AS Date), 1)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (5, CAST(N'2019-09-28' AS Date), 1)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (6, CAST(N'2019-11-05' AS Date), 2)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (7, CAST(N'2019-09-11' AS Date), 3)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (8, CAST(N'2019-09-13' AS Date), 3)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (9, CAST(N'2019-09-14' AS Date), 3)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (10, CAST(N'2019-09-18' AS Date), 4)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (11, CAST(N'2019-09-19' AS Date), 4)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (12, CAST(N'2019-09-20' AS Date), 4)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (13, CAST(N'2019-09-21' AS Date), 4)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (14, CAST(N'2019-09-18' AS Date), 5)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (15, CAST(N'2019-09-19' AS Date), 5)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (16, CAST(N'2019-09-20' AS Date), 5)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (17, CAST(N'2019-09-21' AS Date), 5)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (18, CAST(N'2019-09-27' AS Date), 6)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (19, CAST(N'2019-09-07' AS Date), 7)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (23, CAST(N'2019-09-19' AS Date), 8)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (24, CAST(N'2019-09-28' AS Date), 8)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (25, CAST(N'2019-09-18' AS Date), 9)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (26, CAST(N'2019-09-20' AS Date), 9)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (27, CAST(N'2019-09-21' AS Date), 9)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (28, CAST(N'2019-09-25' AS Date), 10)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (29, CAST(N'2019-09-28' AS Date), 10)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (30, CAST(N'2019-09-30' AS Date), 10)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (31, CAST(N'2019-11-09' AS Date), 10)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (32, CAST(N'2019-09-13' AS Date), 11)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (33, CAST(N'2019-09-14' AS Date), 11)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (36, CAST(N'2019-09-13' AS Date), 12)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (37, CAST(N'2019-09-14' AS Date), 12)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (38, CAST(N'2019-09-25' AS Date), 13)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (39, CAST(N'2019-09-11' AS Date), 14)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (40, CAST(N'2019-09-13' AS Date), 14)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (41, CAST(N'2019-09-27' AS Date), 15)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (42, CAST(N'2019-09-18' AS Date), 16)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (43, CAST(N'2019-09-27' AS Date), 16)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (46, CAST(N'2019-09-28' AS Date), 17)
INSERT [dbo].[T_LeaveHistory] ([Id], [Date], [LeaveId]) VALUES (47, CAST(N'2019-09-29' AS Date), 17)
SET IDENTITY_INSERT [dbo].[T_LeaveHistory] OFF
SET IDENTITY_INSERT [dbo].[T_Leaves] ON 

INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (1, 1, N'Ami', NULL, N'Humas', 1, N'Cuti Tahunan', N'cuti taun', NULL, NULL, N'APPROVED', N'Asep', N'Yuwono', CAST(N'2019-09-06' AS Date), CAST(N'2019-09-09' AS Date), N'APPROVED', N'APPROVED', N'NRP1', NULL, NULL, 3, N'SINGLE', N'profile.jpg', NULL, NULL, NULL, NULL)
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (2, 1, N'Ami', NULL, N'Humas', 5, N'Cuti Haid', N'cuti hadi', NULL, NULL, N'APPROVED', N'Asep', N'Yuwono', CAST(N'2019-09-06' AS Date), CAST(N'2019-09-20' AS Date), N'APPROVED', N'APPROVED', N'NRP1', NULL, NULL, 1, N'SINGLE', N'profile.jpg', NULL, NULL, NULL, NULL)
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (3, 1, N'Gatra', NULL, N'Part', 6, N'Cuti Tidak Masuk Kerja', N'test cuti  tidak kerja', NULL, NULL, N'REJECTED', N'Asep', N'Yuwono', CAST(N'2019-09-06' AS Date), CAST(N'2019-09-06' AS Date), N'APPROVED', N'REJECTED', N'NRP2', CAST(N'2019-09-11' AS Date), CAST(N'2019-09-14' AS Date), 3, N'RANGE', N'profile.jpg', NULL, NULL, N'aposdpasod asd asd', NULL)
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (4, 1, N'Gatra', NULL, N'Part', 1, N'Cuti Tahunan', N'test 333333333', NULL, NULL, N'REJECTED', N'Asep', N'Yuwono', CAST(N'2019-09-06' AS Date), NULL, N'REJECTED', NULL, N'NRP2', CAST(N'2019-09-18' AS Date), CAST(N'2019-09-21' AS Date), 4, N'RANGE', N'profile.jpg', NULL, N'jangan cuti', NULL, NULL)
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (6, 1, N'Ami', NULL, N'Humas', 1, N'Cuti Tahunan', N'gggggggggggg', NULL, NULL, N'APPROVED', N'Asep', N'Yuwono', CAST(N'2019-09-06' AS Date), CAST(N'2019-09-16' AS Date), N'APPROVED', N'APPROVED', N'NRP1', NULL, NULL, 1, N'SINGLE', N'profile.jpg', NULL, NULL, NULL, NULL)
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (7, 1, N'Ami', NULL, N'Humas', 1, N'Cuti Tahunan', N'asdasdasd', NULL, NULL, N'APPROVED', N'Asep', N'Yuwono', CAST(N'2019-09-16' AS Date), CAST(N'2019-09-16' AS Date), N'APPROVED', N'APPROVED', N'NRP1', NULL, NULL, 1, N'SINGLE', N'profile.jpg', NULL, NULL, NULL, NULL)
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (8, 1, N'Julian', NULL, N'Part', 1, N'Cuti Tahunan', N'test', NULL, NULL, N'APPROVED', N'Asep', N'Yuwono', CAST(N'2019-09-09' AS Date), CAST(N'2019-09-09' AS Date), N'APPROVED', N'APPROVED', N'NRP3', NULL, NULL, 2, N'SINGLE', N'profile.jpg', NULL, NULL, NULL, NULL)
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (9, 1, N'Ami', NULL, N'Humas', 1, N'Cuti Tahunan', N'test', NULL, NULL, N'REJECTED', N'Asep', N'Yuwono', CAST(N'2019-09-16' AS Date), NULL, N'REJECTED', NULL, N'NRP1', NULL, NULL, 3, N'SINGLE', N'profile.jpg', NULL, NULL, NULL, NULL)
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (10, 1, N'Ami 150', NULL, N'Humas', 1, N'Cuti Tahunan', N'15000000000', NULL, NULL, N'REJECTED', N'Asep', N'Yuwono', CAST(N'2019-09-10' AS Date), NULL, N'REJECTED', NULL, N'NRP150', NULL, NULL, 4, N'SINGLE', N'profile.jpg', NULL, N'ttttttttttttt', NULL, NULL)
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (11, 1, N'Amiedit', NULL, N'test', 9, N'Sickness', N'test', NULL, NULL, N'PENDING', N'Asep', N'Yuwono', NULL, NULL, NULL, NULL, N'NRP1', CAST(N'2019-09-12' AS Date), CAST(N'2019-09-14' AS Date), 2, N'RANGE', N'profile.jpg', NULL, NULL, NULL, N'2019091004525121752274_10207713879244565_4442760751903306845_n.jpg')
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (12, 1, N'Ami', NULL, N'Humas', 9, N'Sickness', N'sssss', NULL, NULL, N'APPROVED', N'Asep', N'Yuwono', CAST(N'2019-09-10' AS Date), CAST(N'2019-09-16' AS Date), N'APPROVED', N'APPROVED', N'sssssssyy', CAST(N'2019-09-12' AS Date), CAST(N'2019-09-14' AS Date), 2, N'RANGE', N'profile.jpg', NULL, NULL, NULL, N'2019091005410410816337_7e2d0990-e914-403f-9ecc-d3cfdf2ac549_1152_864.jpeg')
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (13, 1, N'Ami', NULL, N'Humas', 9, N'Sickness', N'asdasd', NULL, NULL, N'APPROVED', N'Asep', N'Yuwono', CAST(N'2019-09-16' AS Date), CAST(N'2019-09-16' AS Date), N'APPROVED', N'APPROVED', N'sssssssyy', NULL, NULL, 1, N'SINGLE', N'profile.jpg', NULL, NULL, NULL, N'2019091005445642173510_1907675982678947_4544336228236918784_n.jpg')
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (14, 1, N'Ami', NULL, N'Humas', 9, N'Sickness', N'sakittttttttttt', NULL, NULL, N'PENDING', N'Asep', N'Yuwono', NULL, NULL, NULL, NULL, N'NRP1', CAST(N'2019-09-11' AS Date), CAST(N'2019-09-13' AS Date), 2, N'RANGE', N'profile.jpg', NULL, NULL, NULL, N'2019091007051510816337_7e2d0990-e914-403f-9ecc-d3cfdf2ac549_1152_864.jpeg')
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (15, 1, N'Sasa', NULL, N'Humas', 9, N'Sickness', N'test', NULL, NULL, N'REJECTED', N'Asep', N'Yuwono', CAST(N'2019-09-16' AS Date), NULL, N'REJECTED', NULL, N'NRP244', NULL, NULL, 1, N'SINGLE', N'profile.jpg', NULL, N'gagal', NULL, N'201909110958466d6c224641cdcff679e89c90ba2f7b8c.jpg')
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (16, 1, N'sdsd', NULL, N'sdsd', 9, N'Sickness', N'zxz', NULL, NULL, N'REJECTED', N'Asep', N'Yuwono', CAST(N'2019-09-16' AS Date), NULL, N'REJECTED', NULL, N'dsds', NULL, NULL, 2, N'SINGLE', N'profile.jpg', NULL, N'test', NULL, N'20190911103245test2 - Copy.pdf')
INSERT [dbo].[T_Leaves] ([Id], [UserId], [UserName], [SectionId], [SectionName], [LeaveTypeId], [LeaveTypeName], [Reason], [FormNumber], [ReffNumber], [Status], [App1], [App2], [AppDate1], [AppDate2], [App1Status], [App2Status], [Nrp], [StartDate], [EndDate], [LeaveUsed], [DatePickType], [UserImage], [RejectReason], [RejectReason1], [RejectReason2], [Attachment]) VALUES (17, 1, N'Ami', NULL, N'Humasddddddddddd', 9, N'Sickness', N'flu', NULL, NULL, N'APPROVED', N'Asep', N'Yuwono', CAST(N'2019-09-16' AS Date), CAST(N'2019-09-16' AS Date), N'APPROVED', N'APPROVED', N'NRP1', NULL, NULL, 2, N'SINGLE', N'profile.jpg', NULL, NULL, NULL, N'20190916100420-awWZ3n2m_700w_0.jpg')
SET IDENTITY_INSERT [dbo].[T_Leaves] OFF
SET IDENTITY_INSERT [dbo].[T_Overtime] ON 

INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (3, N'Ami', N'NRP1', N'Humas', CAST(N'2019-09-14' AS Date), CAST(N'18:00:00' AS Time), CAST(N'22:00:00' AS Time), N'test11111', N'Tata', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'APPROVED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (4, N'Gatra', N'NRP2', N'Part', CAST(N'2019-09-30' AS Date), CAST(N'19:30:00' AS Time), CAST(N'22:30:00' AS Time), N'test333333333', N'Tata', N'REJECTED', CAST(N'2019-09-09' AS Date), N'jangan dulu lembur', N'Jaja', NULL, NULL, NULL, N'REJECTED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (5, N'Ajohan', N'NRP3', N'Part', CAST(N'2019-09-27' AS Date), CAST(N'22:00:00' AS Time), CAST(N'23:00:00' AS Time), N'test ggggggg66666666677777', N'Tata', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', N'REJECTED', CAST(N'2019-09-09' AS Date), N'gggggg', N'REJECTED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (6, N'gg', N'NRP555', N'asdasd', CAST(N'2019-09-11' AS Date), CAST(N'01:30:00' AS Time), CAST(N'22:00:00' AS Time), N'gg', N'Tata', N'REJECTED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', NULL, NULL, NULL, N'REJECTED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (7, N'gg', N'NRP555', N'asdasd', CAST(N'2019-09-07' AS Date), CAST(N'03:30:00' AS Time), CAST(N'22:00:00' AS Time), N'gf', N'Tata', N'REJECTED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', NULL, NULL, NULL, N'REJECTED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (8, N'gg', N'NRP555', N'asdasd', CAST(N'2019-09-26' AS Date), CAST(N'18:30:00' AS Time), CAST(N'19:00:00' AS Time), N'sdfsd', N'Tata', N'REJECTED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', NULL, NULL, NULL, N'REJECTED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (9, N'dfgdfg', N'NRP2', N'dfgdfg', CAST(N'2019-09-18' AS Date), CAST(N'21:00:00' AS Time), CAST(N'23:00:00' AS Time), N'dfgdfg', N'Tata', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'APPROVED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (10, N'dfgdfgdw4', N'rtyrty', N'rtyrty', CAST(N'2019-08-31' AS Date), CAST(N'01:00:00' AS Time), CAST(N'23:00:00' AS Time), N'dfgdfgdfg', N'Tata', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', N'REJECTED', CAST(N'2019-09-10' AS Date), N'asdasdasd', N'REJECTED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (12, N'Amiedit', N'NRP1', N'Humas', CAST(N'2019-09-13' AS Date), CAST(N'01:00:00' AS Time), CAST(N'02:00:00' AS Time), N'test jam 4 l4bih', N'Tata', NULL, NULL, NULL, N'Jaja', NULL, NULL, NULL, N'PENDING', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (13, N'Amiedit', N'NRP1', N'Humas', CAST(N'2019-09-14' AS Date), CAST(N'01:00:00' AS Time), CAST(N'02:00:00' AS Time), N'lebih dari 4 22222', N'Tata', NULL, NULL, NULL, N'Jaja', NULL, NULL, NULL, N'PENDING', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (14, N'Joko', N'NRP244', N'Humas', CAST(N'2019-09-18' AS Date), CAST(N'18:00:00' AS Time), CAST(N'21:30:00' AS Time), N'mengerjakan laporan', N'Tata', N'APPROVED', CAST(N'2019-09-16' AS Date), NULL, N'Jaja', N'APPROVED', CAST(N'2019-09-16' AS Date), NULL, N'APPROVED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (15, N'Ami7777', N'NRP177777', N'777', CAST(N'2019-09-07' AS Date), CAST(N'01:00:00' AS Time), CAST(N'03:00:00' AS Time), N'777777', N'Tata', N'REJECTED', CAST(N'2019-09-16' AS Date), NULL, N'Jaja', NULL, NULL, NULL, N'REJECTED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (16, N'Ami', N'NRP1', N'Humas', CAST(N'2019-09-16' AS Date), CAST(N'18:00:00' AS Time), CAST(N'20:00:00' AS Time), N'dsfsdfds', N'Tata', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'APPROVED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (17, N'Ami', N'NRP1', N'Humas', CAST(N'2019-09-24' AS Date), CAST(N'20:00:00' AS Time), CAST(N'23:00:00' AS Time), N'test malam malam', N'Tata', N'APPROVED', CAST(N'2019-09-24' AS Date), NULL, N'Jaja', N'APPROVED', CAST(N'2019-09-24' AS Date), NULL, N'APPROVED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (18, N'Ami', N'NRP1', N'Humas', CAST(N'2019-09-24' AS Date), CAST(N'20:00:00' AS Time), CAST(N'23:00:00' AS Time), N'test 2', N'Tata', N'APPROVED', CAST(N'2019-09-24' AS Date), NULL, N'Jaja', N'APPROVED', CAST(N'2019-09-24' AS Date), NULL, N'APPROVED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (19, N'Ami', N'NRP1', N'Humas', CAST(N'2019-09-04' AS Date), CAST(N'16:00:00' AS Time), CAST(N'23:00:00' AS Time), N'waisak gawe', N'Tata', N'APPROVED', CAST(N'2019-09-24' AS Date), NULL, N'Jaja', N'APPROVED', CAST(N'2019-09-24' AS Date), NULL, N'APPROVED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (20, N'Ami', N'NRP1', N'Humas', CAST(N'2019-09-13' AS Date), CAST(N'15:00:00' AS Time), CAST(N'23:00:00' AS Time), N'test cuti masal', N'Tata', N'APPROVED', CAST(N'2019-09-24' AS Date), NULL, N'Jaja', N'APPROVED', CAST(N'2019-09-24' AS Date), NULL, N'APPROVED', N'profile.jpg')
INSERT [dbo].[T_Overtime] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [OverTimeWork], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status], [UserImage]) VALUES (21, N'Ami', N'NRP1', N'Humas', CAST(N'2019-10-03' AS Date), CAST(N'01:00:00' AS Time), CAST(N'02:00:00' AS Time), N'ssssssssssssssssssssss', N'Tata', NULL, NULL, NULL, N'Jaja', NULL, NULL, NULL, N'PENDING', N'profile.jpg')
SET IDENTITY_INSERT [dbo].[T_Overtime] OFF
SET IDENTITY_INSERT [dbo].[T_Permits] ON 

INSERT [dbo].[T_Permits] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [Remark], [UserImage], [Utilities], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status]) VALUES (1, N'Ami', N'NRP1edit', N'Humas', CAST(N'2019-09-18' AS Date), CAST(N'01:00:00' AS Time), CAST(N'02:00:00' AS Time), N'test', N'profile.jpg', N'PRIVATE', N'Jaja', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'APPROVED')
INSERT [dbo].[T_Permits] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [Remark], [UserImage], [Utilities], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status]) VALUES (2, N'Ami', N'NRP1', N'Humas', CAST(N'2019-09-17' AS Date), CAST(N'01:00:00' AS Time), CAST(N'22:00:00' AS Time), N'test', N'profile.jpg', N'WORK', N'Jaja', N'REJECTED', CAST(N'2019-09-09' AS Date), N'rejt', N'Jaja', NULL, NULL, NULL, N'REJECTED')
INSERT [dbo].[T_Permits] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [Remark], [UserImage], [Utilities], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status]) VALUES (4, N'mmm', N'mm', N'tet', CAST(N'2019-09-06' AS Date), CAST(N'00:30:00' AS Time), CAST(N'23:30:00' AS Time), N'rejeckeun', N'profile.jpg', N'PRIVATE', N'Jaja', N'REJECTED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', NULL, NULL, NULL, N'REJECTED')
INSERT [dbo].[T_Permits] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [Remark], [UserImage], [Utilities], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status]) VALUES (5, N'mmm', N'mm', N'tet', CAST(N'2019-09-20' AS Date), CAST(N'00:30:00' AS Time), CAST(N'23:30:00' AS Time), N'test', N'profile.jpg', N'PRIVATE', N'Jaja', N'REJECTED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', NULL, NULL, NULL, N'REJECTED')
INSERT [dbo].[T_Permits] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [Remark], [UserImage], [Utilities], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status]) VALUES (6, N'mmm', N'mm', N'tet', CAST(N'2019-09-07' AS Date), CAST(N'00:30:00' AS Time), CAST(N'23:30:00' AS Time), N'as', N'profile.jpg', N'PRIVATE', N'Jaja', N'REJECTED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', NULL, NULL, NULL, N'REJECTED')
INSERT [dbo].[T_Permits] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [Remark], [UserImage], [Utilities], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status]) VALUES (7, N'ff', N'ff', N'ff', CAST(N'2019-09-02' AS Date), CAST(N'00:30:00' AS Time), CAST(N'23:00:00' AS Time), N'ff', N'profile.jpg', N'PRIVATE', N'Jaja', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', N'REJECTED', CAST(N'2019-09-09' AS Date), NULL, N'REJECTED')
INSERT [dbo].[T_Permits] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [Remark], [UserImage], [Utilities], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status]) VALUES (8, N'ff', N'ff', N'ff', CAST(N'2019-09-06' AS Date), CAST(N'00:30:00' AS Time), CAST(N'23:00:00' AS Time), N'fff', N'profile.jpg', N'PRIVATE', N'Jaja', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'Jaja', N'REJECTED', CAST(N'2019-09-09' AS Date), NULL, N'REJECTED')
INSERT [dbo].[T_Permits] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [Remark], [UserImage], [Utilities], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status]) VALUES (9, N'bbbbbbbb', N'bbbb', N'Humas', CAST(N'2019-09-19' AS Date), CAST(N'01:00:00' AS Time), CAST(N'23:00:00' AS Time), N'vbbbbbbbbbbbb', N'profile.jpg', N'PRIVATE', N'Rara', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'Dadang', N'APPROVED', CAST(N'2019-09-09' AS Date), NULL, N'APPROVED')
INSERT [dbo].[T_Permits] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [Remark], [UserImage], [Utilities], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status]) VALUES (11, N'Ami', N'NRP1', N'Humas', CAST(N'2019-09-11' AS Date), CAST(N'21:00:00' AS Time), CAST(N'23:00:00' AS Time), N'test', N'profile.jpg', N'PRIVATE', N'Rara', N'REJECTED', CAST(N'2019-09-16' AS Date), N'asdasd', N'Dadang', NULL, NULL, NULL, N'REJECTED')
INSERT [dbo].[T_Permits] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [Remark], [UserImage], [Utilities], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status]) VALUES (12, N'Joko', N'NRP44', N'Humas', CAST(N'2019-09-27' AS Date), CAST(N'19:00:00' AS Time), CAST(N'21:30:00' AS Time), N'Ijin benrangkat ketemu klien', N'profile.jpg', N'WORK', N'Rara', NULL, NULL, NULL, N'Dadang', NULL, NULL, NULL, N'PENDING')
INSERT [dbo].[T_Permits] ([Id], [UserName], [Nrp], [SectionName], [Date], [StartTime], [EndTime], [Remark], [UserImage], [Utilities], [App1], [AppStatus1], [AppDate1], [RejectReason1], [App2], [AppStatus2], [AppDate2], [RejectReason2], [Status]) VALUES (13, N'qwe', N'NRPqwe', N'qwe', CAST(N'2019-09-07' AS Date), CAST(N'04:04:00' AS Time), CAST(N'05:05:00' AS Time), N'qwe', N'profile.jpg', N'PRIVATE', N'Rara', N'APPROVED', CAST(N'2019-09-16' AS Date), NULL, N'Dadang', N'APPROVED', CAST(N'2019-09-16' AS Date), NULL, N'APPROVED')
SET IDENTITY_INSERT [dbo].[T_Permits] OFF
SET IDENTITY_INSERT [dbo].[T_TravelExpense_Attachment] ON 

INSERT [dbo].[T_TravelExpense_Attachment] ([Id], [TravelExpenseId], [Name], [Description]) VALUES (1, 2, N'20190918034226-awWZ3n2m_700w_0.jpg', NULL)
INSERT [dbo].[T_TravelExpense_Attachment] ([Id], [TravelExpenseId], [Name], [Description]) VALUES (2, 2, N'20190918034226-ami.jpg', NULL)
INSERT [dbo].[T_TravelExpense_Attachment] ([Id], [TravelExpenseId], [Name], [Description]) VALUES (3, 3, N'20190919091120-awWZ3n2m_700w_0.jpg', NULL)
INSERT [dbo].[T_TravelExpense_Attachment] ([Id], [TravelExpenseId], [Name], [Description]) VALUES (4, 3, N'20190919091120-69346688_1347899555362834_6377318731323801600_n.png', NULL)
INSERT [dbo].[T_TravelExpense_Attachment] ([Id], [TravelExpenseId], [Name], [Description]) VALUES (19, 4, N'20190919013301-ghaida.jpg', NULL)
INSERT [dbo].[T_TravelExpense_Attachment] ([Id], [TravelExpenseId], [Name], [Description]) VALUES (21, 5, N'20190919090453-ami.jpg', NULL)
INSERT [dbo].[T_TravelExpense_Attachment] ([Id], [TravelExpenseId], [Name], [Description]) VALUES (23, 6, N'20190919090453-ami.jpg', NULL)
SET IDENTITY_INSERT [dbo].[T_TravelExpense_Attachment] OFF
SET IDENTITY_INSERT [dbo].[T_TravelExpenses] ON 

INSERT [dbo].[T_TravelExpenses] ([Id], [TravelRererence], [TravelId], [UserName], [Nrp], [SectionName], [SectionId], [Destination], [TravelDate], [ReportDate], [TotalAllowance], [TotalAccomodation], [TotalExpense], [TotalAll], [TotalAdvance], [PaidByCc], [LessPaidToEmployee], [MorePaidToCompany], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [App5], [AppDate5], [RejectReason5], [AppStatus5], [UserImage], [Status], [TotalPerDate]) VALUES (1, N'Reference Number11', 14, N'Ami', N'NRP1', N'Humas', NULL, N'test city - test company', N'2019/09/27 - 2019/09/30', CAST(N'2019-09-18' AS Date), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Tatag', CAST(N'2019-09-19' AS Date), N'sfsdfsd', N'REJECTED', N'Jajag', NULL, NULL, NULL, N'Sopiag', NULL, NULL, NULL, N'Erwag', NULL, NULL, NULL, N'Ronig', NULL, NULL, NULL, N'profile.jpg', N'REJECTED', NULL)
INSERT [dbo].[T_TravelExpenses] ([Id], [TravelRererence], [TravelId], [UserName], [Nrp], [SectionName], [SectionId], [Destination], [TravelDate], [ReportDate], [TotalAllowance], [TotalAccomodation], [TotalExpense], [TotalAll], [TotalAdvance], [PaidByCc], [LessPaidToEmployee], [MorePaidToCompany], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [App5], [AppDate5], [RejectReason5], [AppStatus5], [UserImage], [Status], [TotalPerDate]) VALUES (3, N'Reference Number11', 14, N'Ami', N'NRP1asd', N'Humas', NULL, N'test city - test company', N'2019/09/27 - 2019/09/30', CAST(N'2019-09-19' AS Date), NULL, NULL, 16, NULL, 10, 4, 2, 0, N'Tatag', NULL, NULL, NULL, N'Jajag', NULL, NULL, NULL, N'Sopiag', NULL, NULL, NULL, N'Erwag', NULL, NULL, NULL, N'Ronig', NULL, NULL, NULL, N'profile.jpg', N'PENDING', NULL)
INSERT [dbo].[T_TravelExpenses] ([Id], [TravelRererence], [TravelId], [UserName], [Nrp], [SectionName], [SectionId], [Destination], [TravelDate], [ReportDate], [TotalAllowance], [TotalAccomodation], [TotalExpense], [TotalAll], [TotalAdvance], [PaidByCc], [LessPaidToEmployee], [MorePaidToCompany], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [App5], [AppDate5], [RejectReason5], [AppStatus5], [UserImage], [Status], [TotalPerDate]) VALUES (4, N'Reference Number11', 14, N'Gogoedit', N'NRP13edit', N'Partedit', NULL, N'test city - test company', N'2019/09/27 - 2019/09/30', CAST(N'2019-09-19' AS Date), NULL, NULL, 5064, NULL, 10, 0, 5054, 0, N'Tatag', CAST(N'2019-09-19' AS Date), NULL, N'APPROVED', N'Jajag', CAST(N'2019-09-19' AS Date), NULL, N'APPROVED', N'Sopiag', NULL, NULL, NULL, N'Erwag', NULL, NULL, NULL, N'Ronig', NULL, NULL, NULL, N'profile.jpg', N'PROCESSING', NULL)
INSERT [dbo].[T_TravelExpenses] ([Id], [TravelRererence], [TravelId], [UserName], [Nrp], [SectionName], [SectionId], [Destination], [TravelDate], [ReportDate], [TotalAllowance], [TotalAccomodation], [TotalExpense], [TotalAll], [TotalAdvance], [PaidByCc], [LessPaidToEmployee], [MorePaidToCompany], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [App5], [AppDate5], [RejectReason5], [AppStatus5], [UserImage], [Status], [TotalPerDate]) VALUES (6, N'00001120190919', 20, N'Ami', N'NRP1', N'Humas', NULL, N'Balikpapan - Company bener', N'2019/09/16 - 2019/09/18', CAST(N'2019-09-19' AS Date), NULL, NULL, 200000, NULL, 4000000, 0, 0, 3800000, N'Tatag', NULL, NULL, NULL, N'Jajag', NULL, NULL, NULL, N'Sopiag', NULL, NULL, NULL, N'Erwag', NULL, NULL, NULL, N'Ronig', NULL, NULL, NULL, N'profile.jpg', N'PENDING', NULL)
SET IDENTITY_INSERT [dbo].[T_TravelExpenses] OFF
SET IDENTITY_INSERT [dbo].[T_Travels] ON 

INSERT [dbo].[T_Travels] ([Id], [UserName], [Nrp], [SectionName], [SectionId], [Title], [TravelCategoryId], [TravelCategoryName], [CityOrCountry], [Company], [Purpose], [DateLeaving], [DateReturning], [Period], [AdvanceAmount], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [ReferenceNumber], [IsExpatriate], [Allowance], [UserImage], [Status], [Passport], [Visa], [Attachment], [App5], [AppDate5], [RejectReason5], [AppStatus5], [AirTicket], [AirportTax], [TrainBus], [TaxiFare], [TollFee], [RentalCar], [Hotel], [AllowanceEstimation], [Other1], [Other2], [Other1Text], [Other2Text]) VALUES (8, N'asdasd', N'asdasd', N'asdasd', NULL, N'asdasd', NULL, N'OVERSEAS', N'asdasd', N'asdasd', N'asdas', CAST(N'2019-09-27' AS Date), CAST(N'2019-09-28' AS Date), 1, 345654, N'Tata', CAST(N'2019-09-16' AS Date), N'sfdsfds', N'REJECTED', N'Jaja', NULL, NULL, NULL, N'Sopia', NULL, NULL, NULL, N'Erwa', NULL, NULL, NULL, N'asdasd', 1, 250000, N'profile.jpg', N'REJECTED', NULL, NULL, NULL, N'Roni', NULL, NULL, NULL, 345654, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'')
INSERT [dbo].[T_Travels] ([Id], [UserName], [Nrp], [SectionName], [SectionId], [Title], [TravelCategoryId], [TravelCategoryName], [CityOrCountry], [Company], [Purpose], [DateLeaving], [DateReturning], [Period], [AdvanceAmount], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [ReferenceNumber], [IsExpatriate], [Allowance], [UserImage], [Status], [Passport], [Visa], [Attachment], [App5], [AppDate5], [RejectReason5], [AppStatus5], [AirTicket], [AirportTax], [TrainBus], [TaxiFare], [TollFee], [RentalCar], [Hotel], [AllowanceEstimation], [Other1], [Other2], [Other1Text], [Other2Text]) VALUES (10, N'asdas', N'asdasd', N'asdasd', NULL, N'asdasd', NULL, N'OVERSEAS', N'asdasd', N'asdasd', N'asdasd', CAST(N'2019-09-20' AS Date), CAST(N'2019-10-04' AS Date), 14, 3423, N'Tata', CAST(N'2019-09-16' AS Date), NULL, N'APPROVED', N'Jaja', NULL, NULL, NULL, N'Sopia', NULL, NULL, NULL, N'Erwa', NULL, NULL, NULL, N'asdasd', 1, 250000, N'profile.jpg', N'PROCESSING', NULL, NULL, NULL, N'Roni', NULL, NULL, NULL, 3423, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'')
INSERT [dbo].[T_Travels] ([Id], [UserName], [Nrp], [SectionName], [SectionId], [Title], [TravelCategoryId], [TravelCategoryName], [CityOrCountry], [Company], [Purpose], [DateLeaving], [DateReturning], [Period], [AdvanceAmount], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [ReferenceNumber], [IsExpatriate], [Allowance], [UserImage], [Status], [Passport], [Visa], [Attachment], [App5], [AppDate5], [RejectReason5], [AppStatus5], [AirTicket], [AirportTax], [TrainBus], [TaxiFare], [TollFee], [RentalCar], [Hotel], [AllowanceEstimation], [Other1], [Other2], [Other1Text], [Other2Text]) VALUES (11, N'yyyyyyyyyy', N'yyyyyyyyyy', N'yyyyyyyyy', NULL, N'yyyyyyyyyyyyyyyy', NULL, N'OVERSEAS', N'yyyyyy', N'yyyyyyyyyy', N'yyyyyyyy', CAST(N'2019-09-20' AS Date), CAST(N'2019-09-28' AS Date), 8, 6666666, N'Tata', CAST(N'2019-09-16' AS Date), NULL, N'APPROVED', N'Jaja', CAST(N'2019-09-16' AS Date), NULL, N'APPROVED', N'Sopia', CAST(N'2019-09-16' AS Date), NULL, N'APPROVED', N'Erwa', CAST(N'2019-09-16' AS Date), N'kkkkkkkkkkkkk yyyyyyyyyyyyyyy', N'REJECTED', N'yyyyyyyyyyyy', 1, 250000, N'profile.jpg', N'REJECTED', NULL, NULL, NULL, N'Roni', NULL, NULL, NULL, 6666666, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'')
INSERT [dbo].[T_Travels] ([Id], [UserName], [Nrp], [SectionName], [SectionId], [Title], [TravelCategoryId], [TravelCategoryName], [CityOrCountry], [Company], [Purpose], [DateLeaving], [DateReturning], [Period], [AdvanceAmount], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [ReferenceNumber], [IsExpatriate], [Allowance], [UserImage], [Status], [Passport], [Visa], [Attachment], [App5], [AppDate5], [RejectReason5], [AppStatus5], [AirTicket], [AirportTax], [TrainBus], [TaxiFare], [TollFee], [RentalCar], [Hotel], [AllowanceEstimation], [Other1], [Other2], [Other1Text], [Other2Text]) VALUES (12, N'aaaaaaaaaaaaa', N'aaaaaaaaaaa', N'aaaaaaaaaaaaa', NULL, N'aaaaaaaaaaaa', NULL, N'OVERSEAS', N'aaaaaaaaaa', N'aaaaaaaa', N'aaaaaaaaa', CAST(N'2019-09-13' AS Date), CAST(N'2019-10-04' AS Date), 21, 5000, N'Tata', CAST(N'2019-09-16' AS Date), NULL, N'APPROVED', N'Jaja', CAST(N'2019-09-16' AS Date), NULL, N'APPROVED', N'Sopia', CAST(N'2019-09-16' AS Date), NULL, N'APPROVED', N'Erwa', CAST(N'2019-09-16' AS Date), NULL, N'APPROVED', N'aaaaaaaaaaaa', 1, 250000, N'profile.jpg', N'APPROVED', N'20190913044852-awWZ3n2m_700w_0.jpg', N'20190913045135-ghaida.jpg', N'20190913044903-69346688_1347899555362834_6377318731323801600_n.png', N'Roni', CAST(N'2019-09-16' AS Date), NULL, N'APPROVED', 5000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'')
INSERT [dbo].[T_Travels] ([Id], [UserName], [Nrp], [SectionName], [SectionId], [Title], [TravelCategoryId], [TravelCategoryName], [CityOrCountry], [Company], [Purpose], [DateLeaving], [DateReturning], [Period], [AdvanceAmount], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [ReferenceNumber], [IsExpatriate], [Allowance], [UserImage], [Status], [Passport], [Visa], [Attachment], [App5], [AppDate5], [RejectReason5], [AppStatus5], [AirTicket], [AirportTax], [TrainBus], [TaxiFare], [TollFee], [RentalCar], [Hotel], [AllowanceEstimation], [Other1], [Other2], [Other1Text], [Other2Text]) VALUES (13, N'ppppp', N'ppppppp', N'pppppp', NULL, N'pppppp', NULL, N'DOMESTIC', N'ppp', N'pppppppp', N'ppppp', CAST(N'2019-09-27' AS Date), CAST(N'2019-09-28' AS Date), 1, 7000, N'Tata', CAST(N'2019-09-16' AS Date), NULL, N'APPROVED', N'Jaja', CAST(N'2019-09-16' AS Date), NULL, N'APPROVED', N'Sopia', CAST(N'2019-09-16' AS Date), NULL, N'REJECTED', N'Erwa', NULL, NULL, NULL, N'pppp', 1, 250000, N'profile.jpg', N'REJECTED', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'')
INSERT [dbo].[T_Travels] ([Id], [UserName], [Nrp], [SectionName], [SectionId], [Title], [TravelCategoryId], [TravelCategoryName], [CityOrCountry], [Company], [Purpose], [DateLeaving], [DateReturning], [Period], [AdvanceAmount], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [ReferenceNumber], [IsExpatriate], [Allowance], [UserImage], [Status], [Passport], [Visa], [Attachment], [App5], [AppDate5], [RejectReason5], [AppStatus5], [AirTicket], [AirportTax], [TrainBus], [TaxiFare], [TollFee], [RentalCar], [Hotel], [AllowanceEstimation], [Other1], [Other2], [Other1Text], [Other2Text]) VALUES (14, N'Ami', N'NRP1', N'Humas', NULL, N'Title', NULL, N'DOMESTIC', N'test city', N'test company', N'test', CAST(N'2019-09-27' AS Date), CAST(N'2019-09-30' AS Date), 3, 10, N'Tata', NULL, NULL, NULL, N'Jaja', NULL, NULL, NULL, N'Sopia', NULL, NULL, NULL, N'Erwa', NULL, NULL, NULL, N'Reference Number11', 1, 250000, N'profile.jpg', N'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2000000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40000, N'', N'bayar gojek')
INSERT [dbo].[T_Travels] ([Id], [UserName], [Nrp], [SectionName], [SectionId], [Title], [TravelCategoryId], [TravelCategoryName], [CityOrCountry], [Company], [Purpose], [DateLeaving], [DateReturning], [Period], [AdvanceAmount], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [ReferenceNumber], [IsExpatriate], [Allowance], [UserImage], [Status], [Passport], [Visa], [Attachment], [App5], [AppDate5], [RejectReason5], [AppStatus5], [AirTicket], [AirportTax], [TrainBus], [TaxiFare], [TollFee], [RentalCar], [Hotel], [AllowanceEstimation], [Other1], [Other2], [Other1Text], [Other2Text]) VALUES (15, N'Ami', N'NRP1', N'HUmas', NULL, N'titel', NULL, N'DOMESTIC', N'city', N'company', N'prpose', CAST(N'2019-09-18' AS Date), CAST(N'2019-09-26' AS Date), 8, 500000, N'Tata', NULL, NULL, NULL, N'Jaja', NULL, NULL, NULL, N'Sopia', NULL, NULL, NULL, N'Erwa', NULL, NULL, NULL, N'test44444', 1, 250000, N'profile.jpg', N'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200000, NULL, NULL, NULL, NULL, NULL, 300000, NULL, NULL, NULL, N'', N'')
INSERT [dbo].[T_Travels] ([Id], [UserName], [Nrp], [SectionName], [SectionId], [Title], [TravelCategoryId], [TravelCategoryName], [CityOrCountry], [Company], [Purpose], [DateLeaving], [DateReturning], [Period], [AdvanceAmount], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [ReferenceNumber], [IsExpatriate], [Allowance], [UserImage], [Status], [Passport], [Visa], [Attachment], [App5], [AppDate5], [RejectReason5], [AppStatus5], [AirTicket], [AirportTax], [TrainBus], [TaxiFare], [TollFee], [RentalCar], [Hotel], [AllowanceEstimation], [Other1], [Other2], [Other1Text], [Other2Text]) VALUES (16, N'Gatra', N'NRP2', N'Part', NULL, N'titl gatra', NULL, N'DOMESTIC', NULL, N'sdf', N'test gatra', CAST(N'2019-09-17' AS Date), CAST(N'2019-09-18' AS Date), 2, 3000, N'Tata', NULL, NULL, NULL, N'Jaja', NULL, NULL, NULL, N'Sopia', NULL, NULL, NULL, N'Erwa', NULL, NULL, NULL, N'', 1, 250000, N'profile.jpg', N'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'')
INSERT [dbo].[T_Travels] ([Id], [UserName], [Nrp], [SectionName], [SectionId], [Title], [TravelCategoryId], [TravelCategoryName], [CityOrCountry], [Company], [Purpose], [DateLeaving], [DateReturning], [Period], [AdvanceAmount], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [ReferenceNumber], [IsExpatriate], [Allowance], [UserImage], [Status], [Passport], [Visa], [Attachment], [App5], [AppDate5], [RejectReason5], [AppStatus5], [AirTicket], [AirportTax], [TrainBus], [TaxiFare], [TollFee], [RentalCar], [Hotel], [AllowanceEstimation], [Other1], [Other2], [Other1Text], [Other2Text]) VALUES (17, N'Ami', N'NRP1', N'Humas', NULL, N'asd', NULL, N'DOMESTIC', N'Bandung', N'company', N'asd', CAST(N'2019-09-26' AS Date), CAST(N'2019-09-28' AS Date), 3, 2000, N'Tata', NULL, NULL, NULL, N'Jaja', NULL, NULL, NULL, N'Sopia', NULL, NULL, NULL, N'Erwa', NULL, NULL, NULL, N'', 1, 250000, N'profile.jpg', N'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20000, NULL, NULL, NULL, 995, NULL, NULL, NULL, NULL, N'', N'')
INSERT [dbo].[T_Travels] ([Id], [UserName], [Nrp], [SectionName], [SectionId], [Title], [TravelCategoryId], [TravelCategoryName], [CityOrCountry], [Company], [Purpose], [DateLeaving], [DateReturning], [Period], [AdvanceAmount], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [ReferenceNumber], [IsExpatriate], [Allowance], [UserImage], [Status], [Passport], [Visa], [Attachment], [App5], [AppDate5], [RejectReason5], [AppStatus5], [AirTicket], [AirportTax], [TrainBus], [TaxiFare], [TollFee], [RentalCar], [Hotel], [AllowanceEstimation], [Other1], [Other2], [Other1Text], [Other2Text]) VALUES (18, N'Ami', N'NRP1', N'humas', NULL, N'asd', NULL, N'OVERSEAS', N'Balikpapan', N'asd', N'asd', CAST(N'2019-09-18' AS Date), CAST(N'2019-09-19' AS Date), 2, 4000, N'Tata', NULL, NULL, NULL, N'Jaja', NULL, NULL, NULL, N'Sopia', NULL, NULL, NULL, N'Erwa', NULL, NULL, NULL, N'baru di tambah', 1, 250000, N'profile.jpg', N'PENDING', NULL, NULL, NULL, N'Roni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4000, NULL, NULL, NULL, NULL, NULL, N'', N'')
INSERT [dbo].[T_Travels] ([Id], [UserName], [Nrp], [SectionName], [SectionId], [Title], [TravelCategoryId], [TravelCategoryName], [CityOrCountry], [Company], [Purpose], [DateLeaving], [DateReturning], [Period], [AdvanceAmount], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [ReferenceNumber], [IsExpatriate], [Allowance], [UserImage], [Status], [Passport], [Visa], [Attachment], [App5], [AppDate5], [RejectReason5], [AppStatus5], [AirTicket], [AirportTax], [TrainBus], [TaxiFare], [TollFee], [RentalCar], [Hotel], [AllowanceEstimation], [Other1], [Other2], [Other1Text], [Other2Text]) VALUES (19, N'ggggggggg', N'NRP1', N'gggggggg', NULL, N'ggggg', NULL, N'OVERSEAS', N'England', N'gggggggg', N'gggggg', CAST(N'2019-09-05' AS Date), CAST(N'2019-09-06' AS Date), 2, 400000, N'Tata', NULL, NULL, NULL, N'Jaja', NULL, NULL, NULL, N'Sopia', NULL, NULL, NULL, N'Erwa', NULL, NULL, NULL, N'', 1, 250000, N'profile.jpg', N'PENDING', N'20190919040712-ami.jpg', N'20190919040718-awWZ3n2m_700w_0.jpg', NULL, N'Roni', NULL, NULL, NULL, 400000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'')
INSERT [dbo].[T_Travels] ([Id], [UserName], [Nrp], [SectionName], [SectionId], [Title], [TravelCategoryId], [TravelCategoryName], [CityOrCountry], [Company], [Purpose], [DateLeaving], [DateReturning], [Period], [AdvanceAmount], [App1], [AppDate1], [RejectReason1], [AppStatus1], [App2], [AppDate2], [RejectReason2], [AppStatus2], [App3], [AppDate3], [RejectReason3], [AppStatus3], [App4], [AppDate4], [RejectReason4], [AppStatus4], [ReferenceNumber], [IsExpatriate], [Allowance], [UserImage], [Status], [Passport], [Visa], [Attachment], [App5], [AppDate5], [RejectReason5], [AppStatus5], [AirTicket], [AirportTax], [TrainBus], [TaxiFare], [TollFee], [RentalCar], [Hotel], [AllowanceEstimation], [Other1], [Other2], [Other1Text], [Other2Text]) VALUES (20, N'Ami', N'NRP1', N'Humas', NULL, N'Title', NULL, N'DOMESTIC', N'Balikpapan', N'Company bener', N'Purpose', CAST(N'2019-09-16' AS Date), CAST(N'2019-09-18' AS Date), 3, 4000000, N'Tata', CAST(N'2019-09-19' AS Date), NULL, N'APPROVED', N'Jaja', CAST(N'2019-09-19' AS Date), NULL, N'APPROVED', N'Sopia', CAST(N'2019-09-19' AS Date), NULL, N'APPROVED', N'Erwa', CAST(N'2019-09-19' AS Date), NULL, N'APPROVED', N'00001120190919', 1, 250000, N'profile.jpg', N'APPROVED', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4000000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'')
SET IDENTITY_INSERT [dbo].[T_Travels] OFF
SET IDENTITY_INSERT [dbo].[T_UserLeaveQuota] ON 

INSERT [dbo].[T_UserLeaveQuota] ([Id], [UserId], [QuotaLeft], [Year], [Nrp]) VALUES (1, NULL, 5, N'2019', N'NRP1')
INSERT [dbo].[T_UserLeaveQuota] ([Id], [UserId], [QuotaLeft], [Year], [Nrp]) VALUES (2, NULL, 10, N'2019', N'NRP2')
INSERT [dbo].[T_UserLeaveQuota] ([Id], [UserId], [QuotaLeft], [Year], [Nrp]) VALUES (3, NULL, 8, N'2019', N'NRP3')
INSERT [dbo].[T_UserLeaveQuota] ([Id], [UserId], [QuotaLeft], [Year], [Nrp]) VALUES (4, NULL, 10, N'2019', N'NRP150')
INSERT [dbo].[T_UserLeaveQuota] ([Id], [UserId], [QuotaLeft], [Year], [Nrp]) VALUES (5, NULL, 10, N'2019', N'')
INSERT [dbo].[T_UserLeaveQuota] ([Id], [UserId], [QuotaLeft], [Year], [Nrp]) VALUES (6, NULL, 10, N'2019', N'sssssssyy')
INSERT [dbo].[T_UserLeaveQuota] ([Id], [UserId], [QuotaLeft], [Year], [Nrp]) VALUES (7, NULL, 10, N'2019', N'NRP244')
INSERT [dbo].[T_UserLeaveQuota] ([Id], [UserId], [QuotaLeft], [Year], [Nrp]) VALUES (8, NULL, 10, N'2019', N'dsds')
INSERT [dbo].[T_UserLeaveQuota] ([Id], [UserId], [QuotaLeft], [Year], [Nrp]) VALUES (9, NULL, 2, N'2018', N'NRP1')
INSERT [dbo].[T_UserLeaveQuota] ([Id], [UserId], [QuotaLeft], [Year], [Nrp]) VALUES (10, NULL, 1, N'2017', N'NRP1')
SET IDENTITY_INSERT [dbo].[T_UserLeaveQuota] OFF
/****** Object:  StoredProcedure [dbo].[spGetLeftQuota]    Script Date: 2019-09-25 3:36:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetLeftQuota]
AS
IF (MONTH(getdate()) < 6)
BEGIN
	select SUM([QuotaLeft]) as  Quota,[Nrp] 
	from T_UserLeaveQuota
	where ([Year] = YEAR(getdate()) OR [Year] = (YEAR(getdate()) -1)) 
	group by Nrp
END

ELSE
BEGIN
	select SUM([QuotaLeft]) as Quota,[Nrp] 
	from T_UserLeaveQuota
	where ([Year] = YEAR(getdate()))
	group by Nrp
END


GO
USE [master]
GO
ALTER DATABASE [ESS_ATTENDANCE] SET  READ_WRITE 
GO
