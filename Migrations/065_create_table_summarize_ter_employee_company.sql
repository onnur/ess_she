CREATE TABLE SummarizeTravelExpenseEmployee (
    Id int IDENTITY(1,1) PRIMARY KEY,
    SummarizeNo varchar(255) NOT NULL,
    JvNumber varchar(255),
    DcNumber varchar(255),
    Total int,
	CreatedAt datetime,
	UpdateAt datetime,
	Nrp varchar(255),
	Title varchar(255),
	Status varchar(255)
);

CREATE TABLE SummarizeTravelExpenseCompany (
    Id int IDENTITY(1,1) PRIMARY KEY,
    SummarizeNo varchar(255) NOT NULL,
    JvNumber varchar(255),
    DcNumber varchar(255),
    Total int,
	CreatedAt datetime,
	UpdateAt datetime,
	Nrp varchar(255),
	Title varchar(255),
	Status varchar(255)
);