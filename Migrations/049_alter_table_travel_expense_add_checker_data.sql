ALTER TABLE [dbo].[T_TravelExpenses] ADD [CheckedNrp] varchar(255);
ALTER TABLE [dbo].[T_TravelExpenses] ADD [CheckedStatus] varchar(255);
ALTER TABLE [dbo].[T_TravelExpenses] ADD [CheckedDate] date;
ALTER TABLE [dbo].[T_TravelExpenses] ADD [CheckedReason] varchar(255);
ALTER TABLE [dbo].[T_TravelExpenses] ADD [CheckedName] varchar(255);
