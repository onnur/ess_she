ALTER TABLE [dbo].[T_Overtime] ADD [CreatedAt] [datetime] NULL;
ALTER TABLE [dbo].[T_Overtime] ADD [UpdatedAt] [datetime] NULL;

ALTER TABLE [dbo].[T_Permits] ADD [CreatedAt] [datetime] NULL;
ALTER TABLE [dbo].[T_Permits] ADD [UpdatedAt] [datetime] NULL;

ALTER TABLE [dbo].[T_Travels] ADD [CreatedAt] [datetime] NULL;
ALTER TABLE [dbo].[T_Travels] ADD [UpdatedAt] [datetime] NULL;

ALTER TABLE [dbo].[T_TravelExpenses] ADD [CreatedAt] [datetime] NULL;
ALTER TABLE [dbo].[T_TravelExpenses] ADD [UpdatedAt] [datetime] NULL;