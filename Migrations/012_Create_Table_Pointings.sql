USE [EmployeeSelfService]
GO
/****** Object:  Table [dbo].[T_Pointings]    Script Date: 2019-09-10 6:13:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Pointings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](max) NULL,
	[Nrp] [varchar](max) NULL,
	[SectionName] [varchar](max) NULL,
	[SectionId] [int] NULL,
	[Amount] [int] NULL,
	[ActivityTypeId] [int] NULL,
	[Remark] [varchar](max) NULL,
	[App1] [varchar](max) NULL,
	[AppDate1] [date] NULL,
	[RejectReason1] [varchar](max) NULL,
	[App2] [varchar](max) NULL,
	[AppDate2] [date] NULL,
	[RejectReason2] [varchar](max) NULL,
	[Status] [varchar](max) NULL,
 CONSTRAINT [PK_T_Pointings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
