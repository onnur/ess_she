USE [EmployeeSelfService]
GO
/****** Object:  Table [dbo].[M_PointingActivity]    Script Date: 2019-09-12 12:50:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_PointingActivity](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
 CONSTRAINT [PK_M_PointingActivity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[M_PointingActivity] ON 

INSERT [dbo].[M_PointingActivity] ([Id], [Name], [Description]) VALUES (1, N'Vacation', N'ticket, accomodation, transportation, airport tax.  ')
INSERT [dbo].[M_PointingActivity] ([Id], [Name], [Description]) VALUES (2, N'Sports', N'gym, sports clothing, sport tools, membership fee')
INSERT [dbo].[M_PointingActivity] ([Id], [Name], [Description]) VALUES (3, N'Education', N'books, school monthly fee, school registration, enrolment fee, privat lesson fee. ')
INSERT [dbo].[M_PointingActivity] ([Id], [Name], [Description]) VALUES (4, N'Self Development', N'training, seminar, language course, computer course, text book.')
INSERT [dbo].[M_PointingActivity] ([Id], [Name], [Description]) VALUES (5, N'Medical', N'eyeglasses, hearing aids, medicine, medical  treatment, therapy, doctor consultancy.')
SET IDENTITY_INSERT [dbo].[M_PointingActivity] OFF
