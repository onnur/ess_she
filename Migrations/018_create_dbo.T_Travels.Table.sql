USE [EmployeeSelfService]
GO
/****** Object:  Table [dbo].[T_Travels]    Script Date: 2019-09-13 5:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Travels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](max) NULL,
	[Nrp] [varchar](max) NULL,
	[SectionName] [varchar](max) NULL,
	[SectionId] [int] NULL,
	[Title] [varchar](max) NULL,
	[TravelCategoryId] [int] NULL,
	[TravelCategoryName] [varchar](max) NULL,
	[CityOrCountry] [varchar](max) NULL,
	[Company] [varchar](max) NULL,
	[Purpose] [varchar](max) NULL,
	[DateLeaving] [date] NULL,
	[DateReturning] [date] NULL,
	[Period] [int] NULL,
	[AdvanceAmount] [int] NULL,
	[App1] [varchar](max) NULL,
	[AppDate1] [date] NULL,
	[RejectReason1] [varchar](max) NULL,
	[AppStatus1] [varchar](max) NULL,
	[App2] [varchar](max) NULL,
	[AppDate2] [date] NULL,
	[RejectReason2] [varchar](max) NULL,
	[AppStatus2] [varchar](max) NULL,
	[App3] [varchar](max) NULL,
	[AppDate3] [date] NULL,
	[RejectReason3] [varchar](max) NULL,
	[AppStatus3] [varchar](max) NULL,
	[App4] [varchar](max) NULL,
	[AppDate4] [date] NULL,
	[RejectReason4] [varchar](max) NULL,
	[AppStatus4] [varchar](max) NULL,
	[ReferenceNumber] [varchar](max) NULL,
	[IsExpatriate] [int] NULL,
	[Allowance] [int] NULL,
	[UserImage] [varchar](max) NULL,
	[Status] [varchar](max) NULL,
	[Passport] [varchar](max) NULL,
	[Visa] [varchar](max) NULL,
	[Attachment] [varchar](max) NULL,
	[App5] [varchar](max) NULL,
	[AppDate5] [date] NULL,
	[RejectReason5] [varchar](max) NULL,
	[AppStatus5] [varchar](max) NULL,
	[AirTicket] [int] NULL,
	[AirportTax] [int] NULL,
	[TrainBus] [int] NULL,
	[TaxiFare] [int] NULL,
	[TollFee] [int] NULL,
	[RentalCar] [int] NULL,
	[Hotel] [int] NULL,
	[AllowanceEstimation] [int] NULL,
	[Other1] [int] NULL,
	[Other2] [int] NULL,
	[Other1Text] [varchar](max) NULL,
	[Other2Text] [varchar](max) NULL,
 CONSTRAINT [PK_T_Travel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
